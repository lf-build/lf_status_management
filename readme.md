# Service Name : lf_status_management

- The purpose of status-management service is to persist the status and workflow of all entities with history.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc LendFoundry.StatusManagement.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore LendFoundry.StatusManagement.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\LendFoundry.StatusManagement.Api\LendFoundry.StatusManagement.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  status-management .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/status-management
	* Sample configurations :
	```
    {
        // This parameter is used to know from where rule will be executed. true means decision-engine will execute rule and false means through product rule
    "UseDecisionEngine": true,
    "entityTypes": {
        // Note below are the entities with their status workflow, which can be multiple as well.
        "application": {
            "StatusWorkFlows": [
                {
                    "Name": "LocApprovalFlow",
                    "Description": "Approve Process for Application",
                    "DefaultInitiateCode": "200.01",
                    "checklist": [
                        {
                            "code": "AdditionalVerification",
                            "title": "Verification completed",
                            "ruleName": "checklistRuleForDynamicFacts",
                            "ruleVersion": "1.0"
                        }
                    ],
                    "statuses": [
                        {
                            "code": "200.01",
                            "name": "ApplicationReceived",
                            "label": "Application Received",
                            "style": "ApplicationReceived",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.02"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.02",
                            "name": "PendingCashflowVerification",
                            "label": "Pending Cashflow Verification",
                            "style": "PendingCashflowVerification",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.03"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.03",
                            "name": "PendingOtherVerifications",
                            "label": "Pending Other Verifications",
                            "style": "PendingOtherVerifications",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.04"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.04",
                            "name": "PendingOfferGeneration",
                            "label": "Pending Offer Generation",
                            "style": "PendingOfferGeneration",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.05"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.05",
                            "name": "OfferGenerated",
                            "label": "Offer Generated",
                            "style": "OfferGenerated",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.06",
                                "200.07",
                                "200.08"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.06",
                            "name": "PendingDocsVerification",
                            "label": "Pending Docs Verification",
                            "style": "PendingDocsVerification",
                            "checklist": [],
                            "transitions": [
                                "200.07",
                                "200.08",
                                "400.01",
                                "400.02",
                                "400.03"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.07",
                            "name": "BoardEscalation",
                            "label": "Board Escalation",
                            "style": "BoardEscalation",
                            "checklist": [
                                "AdditionalVerification"
                            ],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.08"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.08",
                            "name": "OfferPresented",
                            "label": "Offer Presented",
                            "style": "OfferPresented",
                            "checklist": [
                                "AdditionalVerification"
                            ],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.09"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.09",
                            "name": "FraudVerification",
                            "label": "Fraud Verification",
                            "style": "FraudVerification",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.10"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.10",
                            "name": "PendingACHConsent",
                            "label": "Pending ACH Consent",
                            "style": "PendingACHConsent",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.11"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.11",
                            "name": "PendingSignature",
                            "label": "Pending Signature",
                            "style": "PendingSignature",
                            "checklist": [],
                            "transitions": [
                                "400.01",
                                "400.02",
                                "400.03",
                                "200.12",
                                "200.13",
                                "200.15"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.12",
                            "name": "Approved",
                            "label": "Approved",
                            "style": "Approved",
                            "checklist": [],
                            "transitions": [
                                "200.15"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "200.13",
                            "name": "Hold",
                            "label": "Hold",
                            "style": "Hold",
                            "checklist": [],
                            "transitions": [
                                "200.14",
                                "200.15",
                                "400.02"
                            ],
                            "reasons": {
                                "NewBankruptcyFound": "New Bankruptcy found",
                                "NewLienJudgmentsFound": "New Lien/Judgments found",
                                "NewForeclosureFound": "New Foreclosure found",
                                "NegativeCashBalance": "Negative Cash Balance",
                                "OwnerDeathAlert": "Owner's Death Alert",
                                "PaymentFailure": "Payment Failure",
                                "CashflowAccountMismatch": "Cashflow Account Mismatch",
                                "Other": "Other"
                            }
                        },
                        {
                            "code": "200.14",
                            "name": "Closed",
                            "label": "Closed",
                            "style": "Closed",
                            "checklist": [],
                            "transitions": [],
                            "reasons": {}
                        },
                        {
                            "code": "200.15",
                            "name": "Open",
                            "label": "Open",
                            "style": "Open",
                            "checklist": [],
                            "transitions": [
                                "200.13",
                                "200.14",
                                "400.02"
                            ],
                            "reasons": {}
                        },
                        {
                            "code": "400.01",
                            "name": "Declined",
                            "label": "Declined",
                            "style": "Declined",
                            "checklist": [],
                            "transitions": [],
                            "reasons": {
                                "VerificationFailed": "VerificationFailed",
                                "MCADeclines": "MCA Declines",
                                "Fraud": "Fraud",
                                "LiensAndJudgements": "Liens and Judgements",
                                "BKScore": "BK Score",
                                "ThinFileDeclines": "Thin File Declines",
                                "RestrictedIndustry": "RestrictedIndustry",
                                "BelowDeposits": "BelowDeposits",
                                "FICO": "FICO",
                                "PayNet": "PayNet",
                                "HighRisk": "High Risk",
                                "FromCreditModel": "FromCreditModel"
                            }
                        },
                        {
                            "code": "400.02",
                            "name": "NotInterested",
                            "label": "Not Interested",
                            "style": "NotInterested",
                            "checklist": [],
                            "transitions": [],
                            "reasons": {
                                "lesseramount": "Lesser Amount",
                                "otherlender": "Other Lender",
                                "tooslow": "Too Slow",
                                "lessoffer": "Less Offer",
                                "highinterest": "High Interset",
                                "longduration": "Long Duration",
                                "unexpectedemi": "Unexpected EMI",
                                "unexpectedprocessingfee": "High Processing Fee",
                                "unhappyoverall": "Overall not Happy",
                                "other": "Other"
                            }
                        },
                        {
                            "code": "400.03",
                            "name": "Expired",
                            "label": "Expired",
                            "style": "Expired",
                            "checklist": [],
                            "transitions": [],
                            "reasons": {}
                        }
                    ]
                }
            ]
        }
    },
    "dependencies": {
			"lookup": <lookup service URL>,
			"document_manager": <document manager service URL>,
			"template_manager": <template manager service URL>,
		},
		"ConnectionString": <datdabase connection string>,
		"Database": "status-management"
}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by status-management service

- Database : MongoDB
- Collections : status-management, status-data, status-management-history

## External Services called

NA

## Environment Variables used by status-management service

- Environment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different environment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting environment variable in launchSettings.json.
- Below are the environment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\<api_project>\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "status-management",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog