using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public class EntityStatus : Aggregate, IEntityStatus
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string StatusWorkFlowId { get; set; }

        public WorkFlowStatus WorkFlowStatus { get; set; }
        public string Status { get; set; }
        public TimeBucket ActivedOn { get; set; }

        public string ActivatedBy { get; set; }


        public TimeBucket InitiatedDate { get; set; }

        public string InitiatedBy { get; set; }
        public TimeBucket CompletedDate { get; set; }

        public string CompletedBy { get; set; }
        public List<string> Reason { get; set; }
        public string Note { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        public List<ISubStatus> SubStatusDetail { get; set; }
        public string ProductId { get; set; }
        public EntityStatus()
        {

        }

        public EntityStatus(EntityStatus status)
        {
            this.EntityType = status.EntityType;
            this.EntityId = status.EntityId;
            this.Status = status.Status;
            this.Reason = status.Reason;
            this.ActivedOn = status.ActivedOn;
            this.ActivatedBy = status.ActivatedBy;
            this.StatusWorkFlowId = status.StatusWorkFlowId;
            this.InitiatedDate = InitiatedDate;
            this.CompletedBy = CompletedBy;
            this.InitiatedBy = InitiatedBy;
            this.InitiatedDate = InitiatedDate;
            this.WorkFlowStatus = status.WorkFlowStatus;
            this.ProductId = status.ProductId;
        }

    }
}