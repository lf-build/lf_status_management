namespace LendFoundry.StatusManagement
{
    public class Activity : IActivity
    {
        public Activity()
        {
        }

        public Activity(string code, string title)
        {
            Code = code;
            Title = title;
        }

        public string Code { get; set; }
        public string Title { get; set; }
    }
}