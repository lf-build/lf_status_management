﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface ISubWorkFlow
    {
        string Code { get; set; }
        string Name { get; set; }
        string Label { get; set; }
        string Style { get; set; }
        string[] Checklist { get; set; }
        string[] Transitions { get; set; }
        Dictionary<string, string> Activities { get; set; }
        Dictionary<string, string> Reasons { get; set; }
        
        string MoveParentStatus { get; set; }
        bool IsFlowComplete { get; set; }
        Dictionary<string, List<string>> RolesAssignee { get; set; }

    }
}
