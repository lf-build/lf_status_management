﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class SubWorkFlow : ISubWorkFlow
    {
        public SubWorkFlow()
        {
        }

        public SubWorkFlow(SubWorkFlow status)
        {
            Activities = status.Activities;
            Checklist = status.Checklist;
            Code = status.Code;
            Label = status.Label;
            Name = status.Name;
            Style = status.Style;
            Transitions = status.Transitions;
            Reasons = status.Reasons;        
            RolesAssignee = status.RolesAssignee;
         
        }

        public Dictionary<string, string> Activities { get; set; }
        public Dictionary<string, string> Reasons { get; set; }
        public string[] Checklist { get; set; }
        public string Code { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public string[] Transitions { get; set; }

        public Dictionary<string,List<string>> RolesAssignee { get; set; }
       
        public string MoveParentStatus { get; set; }

        public bool IsFlowComplete { get; set; }
     
    }
}
