﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.StatusManagement
{
    public interface IStatusResponse:IStatus
    {
        TimeBucket ActiveOn { get; set; }
        List<string> ReasonsSelected { get; set; }
        string ActivatedBy { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        List<ISubStatus> SubStatusDetail { get; set; }

        string StatusWorkFlowId { get; set; }

        WorkFlowStatus WorkFlowStatus { get; set; }

        string ProductId { get; set; }
    }
}