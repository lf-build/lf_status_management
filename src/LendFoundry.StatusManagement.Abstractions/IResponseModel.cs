﻿using LendFoundry.ProductRule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
     interface IResponseModel
    {
        Result result { get; set; }

        string detail { get; set; }
    }
}
