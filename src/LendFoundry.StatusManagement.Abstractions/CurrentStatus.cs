﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public enum CurrentStatus
    {
    
        Initiated = 0,        
        Completed = 1
    }
}
