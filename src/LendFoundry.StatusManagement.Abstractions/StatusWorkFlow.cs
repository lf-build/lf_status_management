﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class StatusWorkFlow : Aggregate,IStatusWorkFlow
    {
        public string EntityType { get; set; }

        public string EntityId { get; set; }

        public string StatusWorkFlowName { get; set; }

        public WorkFlowStatus WorkFlowStatus { get; set; }

        public TimeBucket InitiatedDate { get; set; }

        public TimeBucket CompletedDate { get; set; }
    }
}
