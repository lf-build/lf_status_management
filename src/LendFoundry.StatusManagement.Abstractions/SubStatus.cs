﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class SubStatus : ISubStatus
    {
        public string SubStatusId { get; set; }
        public string SubStatusCode { get; set; }
        public string SubWorkFlowId { get; set; }
        public WorkFlowStatus SubWorkFlowStatus { get; set; }

        public string ParentStatusCode { get; set; }
        public List<string> Reason { get; set; }

        public TimeBucket ActivedOn { get; set; }
        public string ActivatedBy { get; set; }
        public string Notes { get; set; }

        public TimeBucket InitiatedDate { get; set; }

        public string InitiatedBy { get; set; }
        public TimeBucket CompletedDate { get; set; }

        public string CompletedBy { get; set; }

    }
}
