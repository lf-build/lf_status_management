﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public class StatusChanged
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string OldStatus { get; set; }
        public string NewStatus { get; set; }
        public string OldStatusName { get; set; }
        public string NewStatusName { get; set; }
        public List<string> Reason { get; set; }
        public TimeBucket ActiveOn { get; set; }
        public string Note { get; set; }
        public string StatusWorkFlowId { get; set; }
    }
}
