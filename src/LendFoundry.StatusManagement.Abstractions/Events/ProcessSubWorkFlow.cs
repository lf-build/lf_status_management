﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public class ProcessSubWorkFlow
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string SubWorkFlowId { get; set; }
        public string SubWorkFlowStatus { get; set; }     
        public string NewStatusName { get; set; }
        public List<string> Reason { get; set; }
        public TimeBucket ActiveOn { get; set; }

        public string NewStatus { get; set; }

    }
}
