﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public class SubStatusChanged
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string OldSubStatus { get; set; }
        public string NewSubStatus { get; set; }
        public string OldSubStatusName { get; set; }
        public string NewSubStatusName { get; set; }
        public List<string> Reason { get; set; }
        public TimeBucket ActiveOn { get; set; }
        public string ParentStatus { get; set; }
        public string Note { get; set; }
        public string StatusWorkFlowId { get; set; }
    }
}
