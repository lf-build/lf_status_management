﻿namespace LendFoundry.StatusManagement.Configuration
{
    public class Checklist
    {
        public string Code { get; set; }
        public string RuleName { get; set; }
        public string RuleVersion { get; set; }
        public string Title { get; set; }
    }
}