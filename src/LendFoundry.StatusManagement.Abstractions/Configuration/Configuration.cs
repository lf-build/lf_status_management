﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Configuration
{
    public class Configuration : IConfiguration, IDependencyConfiguration
    {
		public  IDictionary<string, Entity> EntityTypes { get; set; }
        public bool UseDecisionEngine { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}