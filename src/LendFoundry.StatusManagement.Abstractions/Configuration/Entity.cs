﻿using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Configuration
{
	public class Entity
	{
        public List<StatusWorkFlows> StatusWorkFlows { get; set; }
        public string DefaultWorkFlow { get; set; }
    }
}