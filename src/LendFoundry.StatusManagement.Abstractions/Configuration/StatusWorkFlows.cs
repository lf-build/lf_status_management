﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement.Configuration
{
    public class StatusWorkFlows
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DefaultInitiateCode { get; set; }
        public List<Checklist> Checklist { get; set; }
        public List<Status> Statuses { get; set; }

        public string ProductRuleDefination { get; set; }
    }
}
