﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Configuration
{
    public interface IConfiguration : IDependencyConfiguration
    {
        IDictionary<string, Entity> EntityTypes { get; set; }
        bool UseDecisionEngine { get; set; }
        string ConnectionString { get; set; }
    }
}