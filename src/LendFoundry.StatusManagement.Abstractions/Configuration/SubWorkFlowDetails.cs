﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class SubWorkFlowDetails
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Label { get; set; }
        public string DefaultInitiateCode { get; set; }

        public bool CanMoveForward { get; set; }

        public List<SubWorkFlow> Status { get; set; }
    }
}
