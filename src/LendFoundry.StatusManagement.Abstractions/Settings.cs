using System;

namespace LendFoundry.StatusManagement
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "status-management";

    }
}