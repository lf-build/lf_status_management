﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.StatusManagement
{
    public class StatusResponse : Status, IStatusResponse
    {
        public StatusResponse()
        {
        }

        public StatusResponse(IStatus status, IEntityStatus applicationStatus) : base(status == null ? new Status() : status)
        {
            if (applicationStatus != null)
            {
                ActiveOn = applicationStatus.ActivedOn;
                ReasonsSelected = applicationStatus.Reason;
                ActivatedBy = applicationStatus.ActivatedBy;
                SubStatusDetail = applicationStatus.SubStatusDetail;
                StatusWorkFlowId = applicationStatus.StatusWorkFlowId;
                WorkFlowStatus = applicationStatus.WorkFlowStatus;
                ProductId = applicationStatus.ProductId;
            }
        }

        public TimeBucket ActiveOn { get; set; }
        public List<string> ReasonsSelected { get; set; }
        public string ActivatedBy { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        public List<ISubStatus> SubStatusDetail { get; set; }

        public string StatusWorkFlowId { get; set; }

        public WorkFlowStatus WorkFlowStatus { get; set; }

        public string ProductId { get; set; }
    }
}