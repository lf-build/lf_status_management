﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IStatusWorkFlow : IAggregate 
    {
         string EntityType { get; set; }

         string EntityId { get; set; }

         string StatusWorkFlowName { get; set; }

         WorkFlowStatus WorkFlowStatus { get; set; }

         TimeBucket InitiatedDate { get; set; }

         TimeBucket CompletedDate { get; set; }
    }
}
