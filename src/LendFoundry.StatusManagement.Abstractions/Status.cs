﻿using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public class Status:IStatus
    {
        public Status()
        {            
        }

        public Status(IStatus status)
        {
           
                Activities = status.Activities;
                Checklist = status.Checklist;
                Code = status.Code;
                Label = status.Label;
                Name = status.Name;
                Style = status.Style;
                Transitions = status.Transitions;
                Reasons = status.Reasons;
                RolesAssignee = status.RolesAssignee;
                SubWorkFlows = status.SubWorkFlows;
          
        }

        public Dictionary<string, string> Activities { get; set; }
        public Dictionary<string, string> Reasons { get; set; }
        public string[] Checklist { get; set; }
        public string Code { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public string[] Transitions { get; set; }

        public Dictionary<string, List<string>> RolesAssignee { get; set; }
      

        public List<SubWorkFlowDetails> SubWorkFlows { get; set; }
    }
}