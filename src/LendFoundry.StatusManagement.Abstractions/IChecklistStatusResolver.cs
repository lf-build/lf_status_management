namespace LendFoundry.StatusManagement
{
    public interface IDecisionEngine
    {
        TReturn Execute<TParameter, TReturn>(string name, string version, TParameter parameter);
    }
}