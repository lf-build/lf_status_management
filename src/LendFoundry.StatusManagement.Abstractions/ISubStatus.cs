﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface ISubStatus
    {
        string SubStatusId { get; set; }
        string SubStatusCode { get; set; }
        string SubWorkFlowId { get; set; }
        WorkFlowStatus SubWorkFlowStatus { get; set; }
       
        string ParentStatusCode { get; set; }
        List<string> Reason { get; set; }

        TimeBucket ActivedOn { get; set; }
        string ActivatedBy { get; set; }
        string Notes { get; set; } 

        TimeBucket InitiatedDate { get; set; }

        string InitiatedBy { get; set; }
        TimeBucket CompletedDate { get; set; }

        string CompletedBy { get; set; }

     
        
    }
}
