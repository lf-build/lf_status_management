﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public enum WorkFlowStatus
    {
        Initiated,
        Completed
    }
}
