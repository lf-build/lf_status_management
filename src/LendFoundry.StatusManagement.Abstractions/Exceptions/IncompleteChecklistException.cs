﻿using System;

namespace LendFoundry.StatusManagement
{
    public class IncompleteChecklistException : Exception
    {
        public IncompleteChecklistException(string message) : base(message)
        {

        }

        public IncompleteChecklistException(): this("Verification Pending ")
        {

        }
    }
}