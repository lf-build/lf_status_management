﻿using System;

namespace LendFoundry.StatusManagement
{
    public class TransitionNotAllowedException : Exception
    {
        public TransitionNotAllowedException(string message) : base(message)
        {
            
        }

        public TransitionNotAllowedException(): this("Transition not allowed")
        {
            
        }
    }
}