﻿namespace LendFoundry.StatusManagement
{
    public interface ITransition
    {
        string Code { get; set; }
        string Name { get; set; }
    }
}