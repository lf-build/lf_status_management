﻿using LendFoundry.ProductRule;

namespace LendFoundry.StatusManagement
{
    public class ResponseModel : IResponseModel
    {
      public  Result result { get; set; }

       public string detail { get; set; }
    }
}
