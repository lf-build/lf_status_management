﻿namespace LendFoundry.StatusManagement
{
    public class Checklist : IChecklist
    {
        public Checklist()
        {
            
        }

        public Checklist(string code, string title, ResponseModel result)
        {
            Code = code;
            Title = title;
            Result = result;
        }

        public string Code { get; set; }
        public string Title { get; set; }
        public ResponseModel Result { get; set; }
    }
}