﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IStatusWorkflowRequest 
    {
         string StatusWorkFlowName { get; set; }

         WorkFlowStatus WorkflowStatus { get; set; }
    }
}
