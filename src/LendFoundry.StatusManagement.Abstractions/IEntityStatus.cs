using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public interface IEntityStatus : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }

        string StatusWorkFlowId { get; set; }

        WorkFlowStatus WorkFlowStatus { get; set; }
        string Status { get; set; }
        List<string> Reason { get; set; }
        TimeBucket ActivedOn { get; set; }
        string ActivatedBy { get; set; }

        TimeBucket InitiatedDate { get; set; }

        string InitiatedBy { get; set; }
        TimeBucket CompletedDate { get; set; }

        string CompletedBy { get; set; }
        string Note { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        List<ISubStatus> SubStatusDetail { get; set; }

        string ProductId { get; set; }
    }
}