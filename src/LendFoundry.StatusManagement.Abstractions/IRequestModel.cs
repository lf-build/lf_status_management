﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IRequestModel
    {
        string Note { get; set; }

        List<string> reasons { get; set; }
    }
}
