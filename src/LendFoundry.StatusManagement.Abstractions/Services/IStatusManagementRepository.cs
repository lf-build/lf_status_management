using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IStatusManagementRepository 
    {
        Task<IEntityStatus> GetStatus(string entityType, string entityId);
        Task ChangeStatus(IEntityStatus entityStatus);
        Task<ISubStatus> GetSubStatus(IEntityStatus entityDetails, string status);
        Task<IEntityStatus> GetStatusByWorkFlowName(string entityType, string entityId, string statusWorkFlowName);
        Task<IEntityStatus> GetStatusByWorkFlowStatus(string entityType, string entityId, WorkFlowStatus status);
        Task<List<IEntityStatus>> GetStatusByProduct(string entityType, string entityId, string productId);
    }
}