using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IStatusManagementHistoryRepository : IRepository<IEntityStatus>
    {
        Task<List<IEntityStatus>> GetEntityStatusHistory(string entityType, string entityId, string productId);
    }
}