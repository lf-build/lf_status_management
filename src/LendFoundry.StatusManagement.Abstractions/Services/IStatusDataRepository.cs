using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IStatusDataRepository : IRepository<IEntityStatus>
    {
        Task UpdateStatus(IEntityStatus entityStatus);
    }
}