using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public interface IEntityStatusService
    {
        Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId);
        Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId,string statusWorkFlowId);
        Task ChangeStatus(string entityType, string entityId, string newStatus, IRequestModel requestModel);
        Task ChangeStatus(string entityType, string entityId, string statusWorkFlowId, string newStatus, IRequestModel requestModel);
        Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string status);
        Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string statusWorkFlowId, string status);
        Task<IEnumerable<ITransition>> GetTransitions(string entityType, string status);
        Task<IEnumerable<ITransition>> GetTransitions(string entityType, string statusWorkFlowId, string status);
        Task<IEnumerable<IReason>> GetReasons(string entityType, string status);
        Task<IEnumerable<IReason>> GetReasons(string entityType, string statusWorkFlowId, string status);
        Task<IEnumerable<IActivity>> GetActivities(string entityType, string status);
        Task<IEnumerable<IActivity>> GetActivities(string entityType, string statusWorkFlowId, string status);
        Task<Status> GetStatusByStatusCode(string entityType, string statusWorkFlowId, string status);
        Task<List<IEntityStatus>> GetStatusTransitionHistory(string entityType, string entityId, string productId = null);
        Task InitiateSubWorkFlow(string entityType, string entityId, string subWorkFlowId);
        Task MoveSubWorkFlow(string entityType, string entityId, string statusWorkFlowId, string parentStatus, string subStatus, IRequestModel requestModel);
        Task<IEnumerable<string>> GetWorkFlowNames(string entityType);
        Task<IStatusResponse> GetActiveStatusWorkFlow(string entityType, string entityId);
        Task ProcessStatusWorkFlow(string entityType, string entityId,string productId, string statusWorkFlowId, WorkFlowStatus status);
        Task<ISubWorkFlow> GetSubStatusDetails(string entityType, string statusWorkFlowId, string parentStatus, string subWorkFlowId, string subStatus);
        Task<List<IEntityStatus>> GetStatusByProduct(string entityType, string entityId, string productId);
    }
}