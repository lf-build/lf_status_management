﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class StatusWorkflowRequest : IStatusWorkflowRequest
    {
        public string StatusWorkFlowName { get; set; }

        public WorkFlowStatus WorkflowStatus { get; set; }
    }
}
