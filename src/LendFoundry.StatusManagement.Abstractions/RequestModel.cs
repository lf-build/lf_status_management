﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement
{
    public class RequestModel : IRequestModel
    {
        public string Note { get; set; }
        public List<string> reasons { get; set; }
    }
}
