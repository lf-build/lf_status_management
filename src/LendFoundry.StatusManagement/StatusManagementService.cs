﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Configuration;

namespace LendFoundry.StatusManagement
{
    public class StatusManagementService : IEntityStatusService
    {
        #region Constructor

        public StatusManagementService
            (

                IStatusManagementRepository entityStatusRepository,
                IConfiguration configuration,
                IEventHubClient eventHubClient,
                ILookupService lookup,
                ITenantTime tenantTime,
                ILogger logger,
                IStatusDataRepository statusDataRepository,
                ITokenReader tokenReader,
                ITokenHandler tokenHandler,
                IStatusManagementHistoryRepository statusManagementHistoryRepository,
                IIdentityService identityService, IDecisionEngineService decisionEngineService
            )
        {
            if (identityService == null) throw new ArgumentException($"{nameof(identityService)} is mandatory");
            if (entityStatusRepository == null) throw new ArgumentException($"{nameof(entityStatusRepository)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (eventHubClient == null) throw new ArgumentException($"{nameof(eventHubClient)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (statusDataRepository == null) throw new ArgumentException($"{nameof(statusDataRepository)} is mandatory");
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            if (statusManagementHistoryRepository == null) throw new ArgumentException($"{nameof(statusManagementHistoryRepository)} is mandatory");

            EntityStatusRepository = entityStatusRepository;
            Configuration = configuration;
            EventHubClient = eventHubClient;
            Lookup = lookup;
            TenantTime = tenantTime;
            Logger = logger;
            StatusDataRepository = statusDataRepository;
            TokenReader = tokenReader;
            TokenHandler = tokenHandler;
            StatusManagementHistoryRepository = statusManagementHistoryRepository;
            IdentityService = identityService;
            DecisionEngineService = decisionEngineService;
        }

        #endregion

        #region Variables

        private IIdentityService IdentityService { get; }
        private ILookupService Lookup { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }
        private IStatusManagementRepository EntityStatusRepository { get; }
        private IStatusDataRepository StatusDataRepository { get; }
        private IConfiguration Configuration { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IStatusManagementHistoryRepository StatusManagementHistoryRepository { get; }
        private Dictionary<string, string> EntityTypes { get; set; }
        private string ProductId = "default";
        #endregion

        public async Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);
            return await GetStatusByEntity(entityType, entityId, statusWorkFlowId);
        }

        public async Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId, string statusWorkFlowId)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            var applicationStatus = await EntityStatusRepository.GetStatusByWorkFlowName(entityType, entityId, statusWorkFlowId);

            if (applicationStatus == null)
                throw new NotFoundException($"Application type {entityType} Or Application Id {entityId} was not found.");

            Status status = null;
            if (!string.IsNullOrEmpty(applicationStatus.Status))
                status = await GetStatusByStatusCode(entityType, applicationStatus.StatusWorkFlowId, applicationStatus.Status);

            var statusResponse = new StatusResponse(status, applicationStatus);

            return statusResponse;
        }

        public async Task<List<IEntityStatus>> GetStatusByProduct(string entityType, string entityId, string productId)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            return await EntityStatusRepository.GetStatusByProduct(entityType, entityId, productId);
        }

        public async Task ChangeStatus(string entityType, string entityId, string newStatus, IRequestModel requestModel)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);

            var entityStatus = await EntityStatusRepository.GetStatusByWorkFlowName(entityType, entityId, statusWorkFlowId);
            if (entityStatus == null)
                await ProcessStatusWorkFlow(entityType, entityId, ProductId, statusWorkFlowId, WorkFlowStatus.Initiated);
            else
                await ChangeStatus(entityType, entityId, statusWorkFlowId, newStatus, requestModel);
        }

        public async Task ChangeStatus(string entityType, string entityId, string statusWorkFlowId, string newStatus, IRequestModel requestModel)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            var entityStatus = await EntityStatusRepository.GetStatusByWorkFlowName(entityType, entityId, statusWorkFlowId);
            if (entityStatus == null)
                throw new InvalidOperationException($"StatusWorkFlow {statusWorkFlowId} is not initiated for entityId {entityId}, You can not perform this operation");

            if (entityStatus.WorkFlowStatus == WorkFlowStatus.Completed)
                throw new InvalidOperationException($"StatusWorkFlow {statusWorkFlowId} is completed for entityId {entityId}, You can not perform this operation");

            await GetStatusByStatusCode(entityType, statusWorkFlowId, newStatus);

            // This is to check if Parent status can move to next step or not
            if (entityStatus.SubStatusDetail != null)
            {
                var initiatedDetails = entityStatus.SubStatusDetail.Where(i => i.SubWorkFlowStatus == WorkFlowStatus.Initiated).ToList();
                foreach (var item in initiatedDetails)
                {
                    var subWorkFlowDetails = await GetSubWorkFlowDetails(entityType, statusWorkFlowId, entityStatus.Status, item.SubWorkFlowId);
                    if (subWorkFlowDetails != null)
                    {
                        if (!subWorkFlowDetails.CanMoveForward)
                            throw new ArgumentException("Status can not be changed when subworkflow is in initiated stage");
                    }
                }
            }

            Status oldStatus = null;
            Status newStatusEntity = null;
            var reasons = requestModel?.reasons;
            if (!string.IsNullOrEmpty(entityStatus.Status))
            {
                oldStatus = await GetStatusByStatusCode(entityType, statusWorkFlowId, entityStatus.Status);

                if (!oldStatus.Transitions.Contains(newStatus))
                    throw new TransitionNotAllowedException();

                newStatusEntity = CheckReason(entityType, statusWorkFlowId, newStatus, ref reasons);

                if (newStatusEntity.RolesAssignee != null)
                    CheckPermission(entityStatus.Status, newStatus, newStatusEntity.RolesAssignee);

                var checkList = await GetChecklist(entityType, entityId, statusWorkFlowId, newStatusEntity.Code);

                if (checkList != null && checkList.Any(c => c.Result.result == Result.Failed))
                {
                    var result = checkList.FirstOrDefault(i => i.Result.result == Result.Failed);
                    if (result != null)
                        throw new IncompleteChecklistException(result.Result.detail);
                }
            }
            else
                newStatusEntity = CheckReason(entityType, statusWorkFlowId, newStatus, ref reasons);

            entityStatus.Status = newStatus;
            entityStatus.Reason = reasons;
            entityStatus.Note = requestModel?.Note;
            entityStatus.ActivedOn = new TimeBucket(TenantTime.Now);
            entityStatus.ActivatedBy = ExtractCurrentUser();
            await EntityStatusRepository.ChangeStatus(entityStatus);

            // Add data to history
            StatusManagementHistoryRepository.Add(entityStatus);

            // Add/Update status to status data 
            await StatusDataRepository.UpdateStatus(entityStatus);

            var newStatusName = newStatusEntity?.Name;
            var eventName = UppercaseFirst($"{entityType}StatusChanged");

            List<string> reasonDesc = null;
            if (reasons != null)
                if (newStatusEntity != null)
                    reasonDesc = newStatusEntity.Reasons.Where(x => reasons.Contains(x.Key))?.Select(y => y.Value)
                    .ToList();

            await EventHubClient.Publish(eventName, new StatusChanged
            {
                EntityType = entityType,
                EntityId = entityId,
                OldStatus = oldStatus?.Code,
                OldStatusName = oldStatus?.Name,
                NewStatus = newStatus,
                NewStatusName = newStatusName,
                Reason = reasonDesc,
                ActiveOn = new TimeBucket(TenantTime.Now),
                Note = entityStatus.Note,
                StatusWorkFlowId = statusWorkFlowId
            });
        }

        public async Task InitiateSubWorkFlow(string entityType, string entityId, string subWorkFlowId)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            var statusDetails = await EntityStatusRepository.GetStatusByWorkFlowStatus(entityType, entityId, WorkFlowStatus.Initiated);

            if (statusDetails == null)
                throw new InvalidOperationException("There is no parent workflow in initiated stage , You can not initiate subworkflow");

            var subWorkFlowDetails = await GetSubWorkFlowDetails(entityType, statusDetails.StatusWorkFlowId, statusDetails.Status, subWorkFlowId);

            ISubStatus entitySubStatus = null;

            if (statusDetails.SubStatusDetail != null)
                entitySubStatus = statusDetails.SubStatusDetail.FirstOrDefault(i => i.SubWorkFlowId == subWorkFlowId && i.SubWorkFlowStatus == WorkFlowStatus.Initiated);

            if (entitySubStatus != null)
                throw new InvalidOperationException($"subWorkFlow {subWorkFlowId} is already initiated for the entityId {entityId}");

            SetSubStatusDetails(subWorkFlowId, statusDetails, subWorkFlowDetails);

            await EntityStatusRepository.ChangeStatus(statusDetails);

            // Add data to history
            StatusManagementHistoryRepository.Add(statusDetails);

            // Add/Update status to status data 
            await StatusDataRepository.UpdateStatus(statusDetails);
            SubWorkFlow subStatus = null;
            if (!string.IsNullOrEmpty(subWorkFlowDetails.DefaultInitiateCode))
            {

                subStatus = subWorkFlowDetails.Status.FirstOrDefault(s => string.Equals(s.Code, subWorkFlowDetails.DefaultInitiateCode, StringComparison.InvariantCultureIgnoreCase));

                if (subStatus == null)
                    throw new ArgumentException($"Invalid Substatus '{subWorkFlowDetails.DefaultInitiateCode}'");

                await MoveSubWorkFlow(entityType, entityId, statusDetails.StatusWorkFlowId, subWorkFlowId, subWorkFlowDetails.DefaultInitiateCode, null);
            }

            var eventName = UppercaseFirst($"{entityType}ProcessSubWorkFlow");
            await EventHubClient.Publish(eventName, new ProcessSubWorkFlow
            {
                EntityType = entityType,
                EntityId = entityId,
                NewStatusName = subStatus != null ? subStatus.Name : string.Empty,
                NewStatus = subWorkFlowDetails.DefaultInitiateCode,
                ActiveOn = new TimeBucket(TenantTime.Now)
            });

        }

        public async Task ProcessStatusWorkFlow(string entityType, string entityId, string productId, string statusWorkFlowId, WorkFlowStatus status)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            productId = productId.ToLower();

            if (!Enum.IsDefined(typeof(WorkFlowStatus), status))
                throw new InvalidEnumArgumentException("The value of argument " + nameof(status) + " is invalid for Enum type WorkFlowStatus");

            var workflowDetails = await EntityStatusRepository.GetStatusByWorkFlowName(entityType, entityId, statusWorkFlowId);

            if (status == WorkFlowStatus.Initiated)
                if (workflowDetails != null && workflowDetails.WorkFlowStatus == status && workflowDetails.StatusWorkFlowId != statusWorkFlowId)
                    throw new InvalidOperationException($"StatusWorkFlow {workflowDetails.StatusWorkFlowId} is already active for entityId {entityId}, You can not initiate other status WorkFlow");

            var statusWorkFlowDetails = GetStatusWorkFlowDetails(entityType, statusWorkFlowId).Result;

            await SetInitiateWorkFlow(entityType, entityId, productId, statusWorkFlowId, statusWorkFlowDetails, status, workflowDetails);
            Status statusDetails = null;
            if (!string.IsNullOrEmpty(statusWorkFlowDetails.DefaultInitiateCode) && status == WorkFlowStatus.Initiated && workflowDetails == null)
            {
                statusDetails = statusWorkFlowDetails.Statuses.FirstOrDefault(s => string.Equals(s.Code, statusWorkFlowDetails.DefaultInitiateCode, StringComparison.InvariantCultureIgnoreCase));

                if (statusDetails == null)
                    throw new ArgumentException($"Invalid status '{statusWorkFlowDetails.DefaultInitiateCode}'");

                await ChangeStatus(entityType, entityId, statusWorkFlowId, statusWorkFlowDetails.DefaultInitiateCode, null);

            }

            var eventName = UppercaseFirst($"{entityType}ProcessStatusWorkFlow");
            await EventHubClient.Publish(eventName, new ProcessStatusWorkFlow
            {
                EntityType = entityType,
                EntityId = entityId,
                NewStatusName = statusDetails != null ? statusDetails.Name : string.Empty,
                NewStatus = statusWorkFlowDetails.DefaultInitiateCode,
                ActiveOn = new TimeBucket(TenantTime.Now)
            });

        }

        public async Task MoveSubWorkFlow(string entityType, string entityId, string statusWorkFlowId, string subWorkFlowId, string subStatus, IRequestModel requestModel)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(statusWorkFlowId))
                throw new ArgumentException($"{nameof(statusWorkFlowId)} is mandatory");

            if (string.IsNullOrWhiteSpace(subWorkFlowId))
                throw new ArgumentException($"{nameof(subWorkFlowId)} is mandatory");

            var statusDetails = await EntityStatusRepository.GetStatusByWorkFlowName(entityType, entityId, statusWorkFlowId);
            ISubStatus entitySubStatus = null;

            if (statusDetails.SubStatusDetail != null)
                entitySubStatus = statusDetails.SubStatusDetail.FirstOrDefault(i => i.SubWorkFlowId == subWorkFlowId && i.SubWorkFlowStatus == WorkFlowStatus.Initiated);

            if (entitySubStatus == null)
                throw new NotFoundException($"There is no SubWorkFlow initiated for this entity, You can not move");

            if (entitySubStatus.SubWorkFlowStatus == WorkFlowStatus.Completed)
                throw new InvalidOperationException($"The subWorkFlow {subWorkFlowId} is already completed for this entity {entityId}");

            var entityInfo = await GetEntity(entityType);
            var statusWorkFlowDetails = entityInfo.StatusWorkFlows.FirstOrDefault(x => x.Name == statusDetails.StatusWorkFlowId);
            if (statusWorkFlowDetails == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusDetails.StatusWorkFlowId}'");

            var parentStatusDetails = statusWorkFlowDetails.Statuses.FirstOrDefault(i => i.Code == entitySubStatus.ParentStatusCode);
            if (parentStatusDetails == null)
                throw new ArgumentException($"Invalid parentStatus Code '{statusDetails.Status}'");

            var subWorkFlowDetails = parentStatusDetails.SubWorkFlows.FirstOrDefault(x => x.Name == subWorkFlowId);
            if (subWorkFlowDetails == null)
                throw new ArgumentException($"Invalid SubWorkFlowName '{subWorkFlowId}'");

            var subStatusDetails = subWorkFlowDetails.Status.FirstOrDefault(s => string.Equals(s.Code, subStatus, StringComparison.InvariantCultureIgnoreCase));
            if (subStatusDetails == null)
                throw new ArgumentException($"Invalid Substatus '{subStatus}'");

            SubWorkFlow oldStatus = null;
            if (!string.IsNullOrEmpty(entitySubStatus.SubStatusCode))
            {
                oldStatus = subWorkFlowDetails.Status.FirstOrDefault(s => string.Equals(s.Code, entitySubStatus.SubStatusCode, StringComparison.InvariantCultureIgnoreCase));
                if (!oldStatus.Transitions.Contains(subStatus))
                    throw new TransitionNotAllowedException();
                if (subStatusDetails.RolesAssignee != null)
                    CheckPermission(entitySubStatus.SubStatusCode, subStatus, subStatusDetails.RolesAssignee);

                SubWorkFlowCheckList(entityType, entityId, statusDetails, statusWorkFlowDetails, subStatusDetails);
                CheckReasonForSubWorkFlow(requestModel != null ? requestModel.reasons : null, subStatusDetails);
            }
            else
                CheckReasonForSubWorkFlow(requestModel != null ? requestModel.reasons : null, subStatusDetails);
            if (subStatusDetails.IsFlowComplete)
            {
                entitySubStatus.SubWorkFlowStatus = WorkFlowStatus.Completed;
                entitySubStatus.CompletedDate = new TimeBucket(TenantTime.Now);
                entitySubStatus.CompletedBy = ExtractCurrentUser();
            }
            entitySubStatus.SubStatusCode = subStatus;
            entitySubStatus.ActivedOn = new TimeBucket(TenantTime.Now);
            entitySubStatus.ActivatedBy = ExtractCurrentUser();
            entitySubStatus.Notes = requestModel != null ? requestModel.Note : null;
            await EntityStatusRepository.ChangeStatus(statusDetails);

            if (!string.IsNullOrEmpty(subStatusDetails.MoveParentStatus) && subStatusDetails.MoveParentStatus != subWorkFlowId)
                await ChangeStatus(entityType, entityId, statusWorkFlowId, subStatusDetails.MoveParentStatus, null);

            // Add data to history
            StatusManagementHistoryRepository.Add(statusDetails);
            // Add/Update status to status data 
            await StatusDataRepository.UpdateStatus(statusDetails);
            var eventName = UppercaseFirst($"{entityType}SubStatusChanged");
            await EventHubClient.Publish(eventName, new SubStatusChanged
            {
                EntityType = entityType,
                EntityId = entityId,
                OldSubStatus = oldStatus?.Code,
                OldSubStatusName = oldStatus?.Name,
                NewSubStatus = subStatus,
                NewSubStatusName = subStatusDetails.Name,
                Reason = requestModel != null ? requestModel.reasons : null,
                ActiveOn = new TimeBucket(TenantTime.Now),
                Note = entitySubStatus.Notes,
                StatusWorkFlowId = statusWorkFlowId
            });

        }

        public async Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);

            return await GetChecklist(entityType, entityId, statusWorkFlowId, status);
        }

        public async Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string statusWorkFlowId, string status)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            var applicationStatus = await GetStatusByEntity(entityType, entityId, statusWorkFlowId);
            var requestedStatus = status;

            if (string.IsNullOrWhiteSpace(requestedStatus))
                requestedStatus = applicationStatus.Code;
            var statusDetail = await GetStatusByStatusCode(entityType, statusWorkFlowId, requestedStatus);

            return await GetChecklist(entityType, entityId, applicationStatus.ProductId, statusWorkFlowId, statusDetail);
        }

        public async Task<IEnumerable<ITransition>> GetTransitions(string entityType, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);
            return await GetTransitions(entityType, statusWorkFlowId, status);
        }

        public async Task<IEnumerable<ITransition>> GetTransitions(string entityType, string statusWorkFlowId, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusDetail = await GetStatusByStatusCode(entityType, statusWorkFlowId, status);

            if (statusDetail.Transitions == null || !statusDetail.Transitions.Any())
                throw new NotFoundException($"Transitions not found for status {status}");

            var entity = await GetEntity(entityType);
            var configurationStatusWorkFlow = entity.StatusWorkFlows.FirstOrDefault(x => x.Name == statusWorkFlowId);

            if (configurationStatusWorkFlow == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusWorkFlowId}'");

            return configurationStatusWorkFlow.Statuses.Where(
                    s => statusDetail.Transitions.Contains(s.Code, StringComparer.InvariantCultureIgnoreCase))
                .Select(s => new Transition(s.Code, s.Name));
        }

        public async Task<IEnumerable<IReason>> GetReasons(string entityType, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);
            return await GetReasons(entityType, statusWorkFlowId, status);
        }

        public async Task<IEnumerable<IReason>> GetReasons(string entityType, string statusWorkFlowId, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusDetail = await GetStatusByStatusCode(entityType, statusWorkFlowId, status);

            if (statusDetail.Reasons == null || !statusDetail.Reasons.Any())
                throw new NotFoundException($"Reasons not found for status {status}");

            return statusDetail.Reasons.Select(r => new Reason(r.Key, r.Value));
        }

        public async Task<IEnumerable<IActivity>> GetActivities(string entityType, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusWorkFlowId = GetStatusWorkFlow(entityType);
            return await GetActivities(entityType, statusWorkFlowId, status);
        }

        public async Task<IEnumerable<IActivity>> GetActivities(string entityType, string statusWorkFlowId, string status)
        {
            entityType = EnsureEntityType(entityType);
            var statusDetail = await GetStatusByStatusCode(entityType, statusWorkFlowId, status);

            if (statusDetail.Activities == null || !statusDetail.Activities.Any())
                throw new NotFoundException($"Activities not found for status {status}");

            return statusDetail.Activities.Select(a => new Activity(a.Key, a.Value));
        }

        public async Task<ISubWorkFlow> GetSubStatusDetails(string entityType, string statusWorkFlowId, string parentStatus, string subWorkFlowId, string subStatus)
        {
            entityType = EnsureEntityType(entityType);

            var parentStatusDetail = await GetStatusByStatusCode(entityType, statusWorkFlowId, parentStatus);

            if (parentStatusDetail.SubWorkFlows == null || !parentStatusDetail.SubWorkFlows.Any())
                throw new NotFoundException($"Parent WorkFlow {parentStatus} has not any subworkflow.");

            return await ValidateSubStatusBySubStatusCode(subWorkFlowId, subStatus, parentStatusDetail);

        }

        public async Task<SubWorkFlow> ValidateSubStatusBySubStatusCode(string subStatusWorkFlowName, string subStatus, IStatus parentStatusDetails)
        {
            await Task.Yield();

            if (string.IsNullOrWhiteSpace(subStatusWorkFlowName))
                throw new ArgumentException($"{nameof(subStatusWorkFlowName)} is mandatory");

            if (string.IsNullOrWhiteSpace(subStatus))
                throw new ArgumentException($"{nameof(subStatus)} is mandatory");

            var subWorkFlowDetails = parentStatusDetails.SubWorkFlows.FirstOrDefault(x => x.Name == subStatusWorkFlowName);
            if (subWorkFlowDetails == null)
                throw new ArgumentException($"Invalid SubWorkFlowName '{subStatusWorkFlowName}'");

            var subStatusDetails = subWorkFlowDetails.Status.FirstOrDefault(s => string.Equals(s.Code, subStatus, StringComparison.InvariantCultureIgnoreCase));
            if (subStatusDetails == null)
                throw new ArgumentException($"Invalid Substatus '{subStatus}'");

            return new SubWorkFlow(subStatusDetails);
        }

        public async Task<Status> GetStatusByStatusCode(string entityType, string statusWorkFlowName, string status)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(statusWorkFlowName))
                throw new ArgumentException($"{nameof(statusWorkFlowName)} is mandatory");

            if (string.IsNullOrWhiteSpace(status))
                throw new ArgumentException($"{nameof(status)} is mandatory");

            var entityInfo = await GetEntity(entityType);

            var configurationStatusWorkFlow = entityInfo.StatusWorkFlows.FirstOrDefault(x => x.Name == statusWorkFlowName);

            if (configurationStatusWorkFlow == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusWorkFlowName}'");

            var configurationParentStatusDetails = configurationStatusWorkFlow.Statuses.FirstOrDefault(i => i.Code == status);

            if (configurationParentStatusDetails == null)
                throw new ArgumentException($"Invalid parentStatus Code '{status}'");

            return new Status(configurationParentStatusDetails);
        }

        public async Task<SubWorkFlowDetails> GetSubWorkFlowDetails(string entityType, string statusWorkFlowName, string parentStatus, string subWorkFlowName)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(statusWorkFlowName))
                throw new ArgumentException($"{nameof(statusWorkFlowName)} is mandatory");

            if (string.IsNullOrWhiteSpace(parentStatus))
                throw new ArgumentException($"{nameof(parentStatus)} is mandatory");

            var entityInfo = await GetEntity(entityType);

            var statusWorkFlowDetails = entityInfo.StatusWorkFlows.FirstOrDefault(x => x.Name == statusWorkFlowName);

            if (statusWorkFlowDetails == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusWorkFlowName}'");

            var parentStatusDetails = statusWorkFlowDetails.Statuses.FirstOrDefault(i => i.Code == parentStatus);

            if (parentStatusDetails == null)
                throw new ArgumentException($"Invalid parentStatus Code '{parentStatus}'");

            if (parentStatusDetails.SubWorkFlows == null)
                throw new ArgumentException($"There is no subworkflow find for parent status {parentStatus}");

            var subWorkFlowDetails = parentStatusDetails.SubWorkFlows.FirstOrDefault(x => x.Name == subWorkFlowName);

            if (subWorkFlowDetails == null)
                throw new ArgumentException($"Invalid SubWorkFlowName '{subWorkFlowName}'");

            return subWorkFlowDetails;
        }

        public async Task<IStatusResponse> GetActiveStatusWorkFlow(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            IStatusResponse statusResponse = null;
            var entityDetail = await EntityStatusRepository.GetStatusByWorkFlowStatus(entityType, entityId, WorkFlowStatus.Initiated);
            if (entityDetail != null)
            {
                if (!string.IsNullOrEmpty(entityDetail.Status))
                {
                    var status = await GetStatusByStatusCode(entityType, entityDetail.StatusWorkFlowId, entityDetail.Status);
                    statusResponse = new StatusResponse(status, entityDetail);
                    return statusResponse;
                }
                else
                {
                    statusResponse = new StatusResponse
                    {
                        StatusWorkFlowId = entityDetail.StatusWorkFlowId,
                        WorkFlowStatus = entityDetail.WorkFlowStatus
                    };
                }
            }
            return statusResponse;
        }

        public async Task<StatusWorkFlows> GetStatusWorkFlowDetails(string entityType, string statusWorkFlowName)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            if (string.IsNullOrWhiteSpace(statusWorkFlowName))
                throw new ArgumentException($"{nameof(statusWorkFlowName)} is mandatory");

            var entityInfo = await GetEntity(entityType);

            var statusWorkFlowDetails = entityInfo.StatusWorkFlows.FirstOrDefault(x => x.Name == statusWorkFlowName);

            if (statusWorkFlowDetails == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusWorkFlowName}'");

            return statusWorkFlowDetails;
        }

        public async Task<List<IEntityStatus>> GetStatusTransitionHistory(string entityType, string entityId, string productId = null)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            return await StatusManagementHistoryRepository.GetEntityStatusHistory(entityType, entityId, productId);
        }

        public async Task<IEnumerable<string>> GetWorkFlowNames(string entityType)
        {

            var entityInfo = await GetEntity(entityType);

            if (entityInfo.StatusWorkFlows == null || entityInfo.StatusWorkFlows.Count == 0)
                throw new NotFoundException($"Status Workflow is not fount for {entityType}");

            return entityInfo.StatusWorkFlows.Select(i => i.Name);
        }

        #region PrivateMethods

        private void SubWorkFlowCheckList(string entityType, string entityId, IEntityStatus statusDetails, StatusWorkFlows statusWorkFlowDetails, SubWorkFlow subStatusDetails)
        {
            var checkList = new List<IChecklist>();
            if (subStatusDetails.Checklist != null)
            {
                foreach (var checklistItem in statusWorkFlowDetails.Checklist.Where(c => subStatusDetails.Checklist.Contains(c.Code, StringComparer.InvariantCultureIgnoreCase)))
                {
                    try
                    {
                        var productRuleResult = DecisionEngineService.Execute<dynamic, ProductRuleResult>(checklistItem.RuleName, new { entityType, entityId });

                        var responseModel = new ResponseModel();
                        if (productRuleResult != null)
                        {
                            responseModel.result = productRuleResult.Result;
                            responseModel.detail = productRuleResult.ResultDetail != null && productRuleResult.ResultDetail.Count > 0 ? productRuleResult.ResultDetail[0].ToString() : string.Empty;
                        }
                        else
                            responseModel.result = Result.Failed;
                        checkList.Add(new Checklist(checklistItem.Code, checklistItem.Title, responseModel));
                    }
                    catch (Exception ex)
                    {
                        var response = new ResponseModel { result = Result.Failed };
                        checkList.Add(new Checklist(checklistItem.Code, checklistItem.Title, response));
                        Logger.Error($"Rule {checklistItem.RuleName} could not be executed, please see decision-engine logs for more information.", ex);
                    }
                }
            }
            if (checkList.Any(c => c.Result.result == Result.Failed))
            {
                var result = checkList.FirstOrDefault(i => i.Result.result == Result.Failed);
                if (result != null)
                    throw new IncompleteChecklistException(result.Result.detail);
            }
        }

        private static void CheckReasonForSubWorkFlow(List<string> reasons, ISubWorkFlow subStatusDetails)
        {
            if (subStatusDetails.Reasons != null && subStatusDetails.Reasons.Any())
            {
                if (reasons == null)
                    throw new TransitionNotAllowedException("Reason is required");

                foreach (var item in reasons)
                {
                    if (string.IsNullOrWhiteSpace(item))
                        throw new TransitionNotAllowedException("Reason is required");

                    if (!subStatusDetails.Reasons.ContainsKey(item))
                        throw new TransitionNotAllowedException($"Reason {item} is not valid");
                }
            }
        }

        private async Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string productId, string statusWorkFlowId, Status status)
        {
            if (entityId == null) throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);

            var checklist = new List<IChecklist>();

            var entityInfo = await GetEntity(entityType);

            var configurationStatusWorkFlow = entityInfo.StatusWorkFlows.FirstOrDefault(x => x.Name == statusWorkFlowId);

            if (configurationStatusWorkFlow == null)
                throw new ArgumentException($"Invalid StatusWorkFlowName '{statusWorkFlowId}'");

            if (configurationStatusWorkFlow.Checklist == null)
                return checklist;

            if (status.Checklist == null)
                return checklist;

            foreach (var checklistItem in configurationStatusWorkFlow.Checklist.Where(c => status.Checklist.Contains(c.Code, StringComparer.InvariantCultureIgnoreCase)))
            {
                try
                {
                    ProductRuleResult productRuleResult = DecisionEngineService.Execute<dynamic, ProductRuleResult>(checklistItem.RuleName, new { entityType, entityId });

                    var responseModel = new ResponseModel();
                    if (productRuleResult != null)
                    {
                        responseModel.result = productRuleResult.Result;
                        responseModel.detail = productRuleResult.ResultDetail != null && productRuleResult.ResultDetail.Count > 0 ? productRuleResult.ResultDetail[0].ToString() : string.Empty;
                    }
                    else
                        responseModel.result = Result.Failed;
                    checklist.Add(new Checklist(checklistItem.Code, checklistItem.Title, responseModel));
                }
                catch (Exception ex)
                {
                    var response = new ResponseModel { result = Result.Failed };
                    checklist.Add(new Checklist(checklistItem.Code, checklistItem.Title, response));
                    Logger.Error($"Rule {checklistItem.RuleName} could not be executed, please see decision-engine logs for more information.", ex);
                }
            }

            return checklist;
        }

        private async Task<string> ValidateStatusWorkFlowName(string entityType, string statusWorkFlowName)
        {
            if (string.IsNullOrWhiteSpace(statusWorkFlowName))
                throw new ArgumentException($"{nameof(statusWorkFlowName)} is mandatory");

            var workFlows = await GetWorkFlowNames(entityType);
            var matchingvalues = workFlows.ToList().FirstOrDefault(i => i.Contains(statusWorkFlowName));
            if (matchingvalues == null)
                throw new NotFoundException($"Status Workflow name is not fount for entityType {entityType}");

            return matchingvalues;
        }

        private async Task<Entity> GetEntity(string entityType)
        {
            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");

            Entity entityConfigurationDetails;
            if (!Configuration.EntityTypes.TryGetValue(entityType, out entityConfigurationDetails))
                throw new NotFoundException($"The {entityType} not found for {entityType}");

            return await Task.FromResult(entityConfigurationDetails);

        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (EntityTypes == null)
                EntityTypes = Lookup.GetLookupEntries("entityTypes");

            if (!EntityTypes.ContainsKey(entityType))
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }

        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }

        private EntityStatus CreateEntityStatus(string entityType, string statusWorkFlowName, string entityId, string newStatus, List<string> reasons, List<ISubStatus> substatusInfo)
        {
            return new EntityStatus
            {
                EntityType = entityType,
                StatusWorkFlowId = statusWorkFlowName,
                EntityId = entityId,
                Reason = reasons,
                Status = newStatus,
                ActivedOn = new TimeBucket(TenantTime.Now),
                ActivatedBy = ExtractCurrentUser(),
                SubStatusDetail = substatusInfo
            };
        }

        private Status CheckReason(string entityType, string statusWorkFlowName, string newStatus, ref List<string> reasons)
        {
            var newStatusEntity = GetStatusByStatusCode(entityType, statusWorkFlowName, newStatus).Result;

            if (newStatusEntity.Reasons != null && newStatusEntity.Reasons.Any())
            {
                if (reasons == null)
                    throw new TransitionNotAllowedException("Reason is required");

                foreach (var item in reasons)
                {
                    if (string.IsNullOrWhiteSpace(item))
                        throw new TransitionNotAllowedException("Reason is required");

                    if (!newStatusEntity.Reasons.ContainsKey(item))
                        throw new TransitionNotAllowedException($"Reason {item} is not valid");
                }
            }
            else
            {
                reasons = null;
            }

            return newStatusEntity;
        }

        private void SetSubStatusDetails(string subWorkFlowId, IEntityStatus statusDetails, SubWorkFlowDetails subWorkFlowDetails)
        {
            var subStatusDetail = new SubStatus
            {
                SubStatusId = Guid.NewGuid().ToString("N"),
                ParentStatusCode = statusDetails.Status,
                ActivedOn = new TimeBucket(TenantTime.Now),
                ActivatedBy = ExtractCurrentUser(),
                SubWorkFlowStatus = WorkFlowStatus.Initiated,
                SubWorkFlowId = subWorkFlowId,
                InitiatedBy = ExtractCurrentUser(),
                InitiatedDate = new TimeBucket(TenantTime.Now)
            };

            if (statusDetails.SubStatusDetail != null)
            {
                statusDetails.SubStatusDetail.Add(subStatusDetail);
            }
            else
            {
                var subStatusList = new List<ISubStatus> { subStatusDetail };
                statusDetails.SubStatusDetail = subStatusList;
            }
        }

        private async Task SetInitiateWorkFlow(string entityType, string entityId, string productId, string statusWorkFlowId, StatusWorkFlows statusWorkFlowDetails, WorkFlowStatus status, IEntityStatus entityStatus)
        {
            if (entityStatus == null)
                entityStatus = new EntityStatus();
            entityStatus.EntityType = entityType;
            entityStatus.StatusWorkFlowId = statusWorkFlowId;
            entityStatus.EntityId = entityId;
            entityStatus.WorkFlowStatus = status;
            entityStatus.ActivedOn = new TimeBucket(TenantTime.Now);
            entityStatus.ActivatedBy = ExtractCurrentUser();
            entityStatus.ProductId = productId;
            if (WorkFlowStatus.Initiated == status)
            {
                entityStatus.InitiatedBy = ExtractCurrentUser();
                entityStatus.InitiatedDate = new TimeBucket(TenantTime.Now);
            }
            else
            {
                entityStatus.CompletedBy = ExtractCurrentUser();
                entityStatus.CompletedDate = new TimeBucket(TenantTime.Now);
            }

            await EntityStatusRepository.ChangeStatus(entityStatus);

            // Add data to history
            StatusManagementHistoryRepository.Add(entityStatus);

            // Add/Update status to status data 
            await StatusDataRepository.UpdateStatus(entityStatus);
        }

        private void CheckPermission(string oldStatus, string newStatus, Dictionary<string, List<string>> rolesAssignee)
        {
            var currentUserRoles = IdentityService.GetUserRoles(ExtractCurrentUser()).Result;
            if (currentUserRoles != null)
            {
                var configRoles = rolesAssignee.Keys.ToArray();
                var rolesExists = currentUserRoles.ToList().Intersect(configRoles).ToList();
                if (rolesExists.Count == 0)
                    throw new InvalidOperationException($"You don't have permission to change status from  {oldStatus} to new status {newStatus}");
                foreach (var item in rolesExists)
                {
                    List<string> assigneeNames = null;
                    if (rolesAssignee.TryGetValue(item, out assigneeNames))
                    {
                        if (assigneeNames.Count > 0 && !assigneeNames.Contains(ExtractCurrentUser()))
                            throw new InvalidOperationException($"You can not do this operation as this is assigned to another person");
                    }
                }
            }
        }

        private string GetStatusWorkFlow(string entityType)
        {
            var entityWiseConfig = Configuration.EntityTypes.Where(c => c.Key == entityType).FirstOrDefault();
            if (entityWiseConfig.Value == null)
                throw new ArgumentException($"Configuration with {entityType} not found");
            if (string.IsNullOrWhiteSpace(entityWiseConfig.Value.DefaultWorkFlow))
                throw new ArgumentException($"Default workflow not available for {entityType}");
            var statusWorkFlowId = entityWiseConfig.Value.DefaultWorkFlow;
            return statusWorkFlowId;
        }

        #endregion
    }
}