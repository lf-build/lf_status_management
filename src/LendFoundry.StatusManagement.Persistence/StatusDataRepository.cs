﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement.Persistence
{
    public class StatusDataRepository : MongoRepository<IEntityStatus, EntityStatus>, IStatusDataRepository
    {
        static StatusDataRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(EntityStatus)))
            {
                BsonClassMap.RegisterClassMap<EntityStatus>(map =>
                {
                    map.AutoMap();
                    var type = typeof(EntityStatus);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }

        public StatusDataRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "status-data")
        {
            CreateIndexIfNotExists("tenant-entityType-id-status-workflow", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending((i => i.StatusWorkFlowId)));
            CreateIndexIfNotExists("entityType", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task UpdateStatus(IEntityStatus entityStatus)
        {
            entityStatus.TenantId = TenantService.Current.Id;

           await Task.Run(()=> Collection.UpdateOne(s =>
               s.TenantId == TenantService.Current.Id &&
               s.EntityType == entityStatus.EntityType &&
               s.EntityId == entityStatus.EntityId &&
               s.StatusWorkFlowId == entityStatus.StatusWorkFlowId,
           new UpdateDefinitionBuilder<IEntityStatus>()
               .Set(a => a.Status, entityStatus.Status)
               .Set(a => a.Reason, entityStatus.Reason)
               .Set(a => a.ActivedOn, entityStatus.ActivedOn)
               .Set(a => a.ActivatedBy, entityStatus.ActivatedBy)
               .Set(a => a.WorkFlowStatus, entityStatus.WorkFlowStatus)
               .Set(a => a.SubStatusDetail, entityStatus.SubStatusDetail)
               .Set(a => a.InitiatedBy, entityStatus.InitiatedBy)
               .Set(a => a.InitiatedDate, entityStatus.InitiatedDate)
               .Set(a => a.CompletedBy, entityStatus.CompletedBy)
               .Set(a => a.CompletedDate, entityStatus.CompletedDate)
               .Set(a=>a.ProductId,entityStatus.ProductId)
           , new UpdateOptions() { IsUpsert = true }));

        }

    }
}
