﻿using System.Linq;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Persistence
{
    public class StatusManagementRepository : MongoRepository<IEntityStatus, EntityStatus>, IStatusManagementRepository
    {
        static StatusManagementRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(EntityStatus)))
            {
                BsonClassMap.RegisterClassMap<EntityStatus>(map =>
                {

                    map.AutoMap();
                    var type = typeof(EntityStatus);
                    map.MapProperty(p => p.WorkFlowStatus).SetSerializer(new EnumSerializer<WorkFlowStatus>(BsonType.String));
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
                BsonClassMap.RegisterClassMap<SubStatus>(map =>
                {
                    map.AutoMap();
                    var type = typeof(SubStatus);
                    map.MapProperty(p => p.SubWorkFlowStatus).SetSerializer(new EnumSerializer<WorkFlowStatus>(BsonType.String));
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }

        public StatusManagementRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "status-management")
        {
            CreateIndexIfNotExists("tenant-entityType-id", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("tenant-entityType-id-workflow", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending((i => i.WorkFlowStatus)));
            CreateIndexIfNotExists("tenant-entityType-id-workflow-product", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending(i => i.StatusWorkFlowId).Ascending((i => i.ProductId)));
            CreateIndexIfNotExists("tenant-entityType-id-status-workflow", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending((i => i.StatusWorkFlowId)));
            CreateIndexIfNotExists("entityType", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityId));
        }


        public async Task<IEntityStatus> GetStatus(string entityType, string entityId)
        {
            return await Task.Run(() => Query.FirstOrDefault(a => a.EntityType == entityType && a.EntityId == entityId));
        }

        public async Task<IEntityStatus> GetStatusByWorkFlowStatus(string entityType, string entityId, WorkFlowStatus status)
        {
            return await Task.Run(() => Query.FirstOrDefault(a => a.EntityType == entityType && a.EntityId == entityId && a.WorkFlowStatus == status));
        }
        public async Task<IEntityStatus> GetStatusByWorkFlowName(string entityType, string entityId, string statusWorkFlowName)
        {
            return await Task.Run(() => Query.FirstOrDefault(a => a.EntityType == entityType && a.EntityId == entityId && a.StatusWorkFlowId == statusWorkFlowName));
        }
        public async Task<List<IEntityStatus>> GetStatusByProduct(string entityType, string entityId, string productId)
        {
            return await Task.Run(() => Query.Where(a => a.EntityType == entityType && a.EntityId == entityId && a.ProductId == productId).ToList());
        }
        public async Task<ISubStatus> GetSubStatus(IEntityStatus entityDetails, string status)
        {
            return await Task.Run(() => entityDetails.SubStatusDetail.FirstOrDefault(i => i.SubStatusCode == status));

        }

        public async Task ChangeStatus(IEntityStatus entityStatus)
        {
            entityStatus.TenantId = TenantService.Current.Id;

            await Task.Run(() => Collection.UpdateOne(s =>
                    s.TenantId == TenantService.Current.Id &&
                    s.EntityType == entityStatus.EntityType &&
                    s.EntityId == entityStatus.EntityId &&
                    s.StatusWorkFlowId == entityStatus.StatusWorkFlowId &&
                    s.ProductId == entityStatus.ProductId,
                new UpdateDefinitionBuilder<IEntityStatus>()
                    .Set(a => a.Status, entityStatus.Status)
                    .Set(a => a.Reason, entityStatus.Reason)
                    .Set(a => a.ActivedOn, entityStatus.ActivedOn)
                    .Set(a => a.ActivatedBy, entityStatus.ActivatedBy)
                    .Set(a => a.WorkFlowStatus, entityStatus.WorkFlowStatus)
                    .Set(a => a.SubStatusDetail, entityStatus.SubStatusDetail)
                    .Set(a => a.InitiatedBy, entityStatus.InitiatedBy)
                    .Set(a => a.InitiatedDate, entityStatus.InitiatedDate)
                    .Set(a => a.CompletedBy, entityStatus.CompletedBy)
                    .Set(a => a.CompletedDate, entityStatus.CompletedDate)
                    .Set(a => a.SubStatusDetail, entityStatus.SubStatusDetail)
                , new UpdateOptions() { IsUpsert = true }));



        }

    }
}
