﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace LendFoundry.StatusManagement.Persistence
{
    public class StatusManagementHistoryRepository : MongoRepository<IEntityStatus, EntityStatus>, IStatusManagementHistoryRepository
    {
        static StatusManagementHistoryRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(EntityStatus)))
            {
                BsonClassMap.RegisterClassMap<EntityStatus>(map =>
                {
                    map.AutoMap();
                    var type = typeof(EntityStatus);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }

        public StatusManagementHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "status-management-history")
        {
            CreateIndexIfNotExists("tenant-entityType-id", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("tenant-entityType-id-product",
                Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId).Ascending(i => i.ProductId));
            CreateIndexIfNotExists("entityType", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<List<IEntityStatus>> GetEntityStatusHistory(string entityType, string entityId, string productId)
        {
            //Get Status history
            if (!string.IsNullOrEmpty(productId))
                return  await Query.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.ProductId == productId).ToListAsync();
            else
                return await Query.Where(x => x.EntityType == entityType && x.EntityId == entityId).ToListAsync();
        }
    }
}
