﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="entityStatusService">The entity status service.</param>
        public ApiController(IEntityStatusService entityStatusService)
        {
            EntityStatusService = entityStatusService;
        }

        private IEntityStatusService EntityStatusService { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Processes the status work flow.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/{productId}/workflow/{statusWorkFlowId}/{status}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ProcessStatusWorkFlow(string entityType, string entityId, string productId, string statusWorkFlowId, WorkFlowStatus status) =>
            await ExecuteAsync(async () =>
            {
                await EntityStatusService.ProcessStatusWorkFlow(entityType, entityId, productId, statusWorkFlowId, status);
                return NoContentResult;
            });

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="newStatus">The new status.</param>
        /// <param name="requestModel">The request model.</param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/{statusWorkFlowId}/{newStatus}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ChangeStatus(string entityType, string entityId, string statusWorkFlowId, string newStatus, [FromBody] RequestModel requestModel)
        {
            try
            {
                return await ExecuteAsync(async () => { await EntityStatusService.ChangeStatus(entityType, entityId, statusWorkFlowId, newStatus, requestModel); return NoContentResult; });
            }
            catch (IncompleteChecklistException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (TransitionNotAllowedException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="newStatus">The new status.</param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{newStatus}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ChangeStatus(string entityType, string entityId, string newStatus, [FromBody] RequestModel requestModel)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await EntityStatusService.ChangeStatus(entityType, entityId, newStatus, requestModel);
                    return NoContentResult;
                });
            }
            catch (IncompleteChecklistException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (TransitionNotAllowedException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Initiates the sub work flow.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="subWorkFlowId">The sub work flow identifier.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/subworkflow/{subWorkFlowId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> InitiateSubWorkFlow(string entityType, string entityId, string subWorkFlowId)
        {
            return await ExecuteAsync(async () => { await EntityStatusService.InitiateSubWorkFlow(entityType, entityId, subWorkFlowId); return NoContentResult; });
        }

        /// <summary>
        /// Moves the sub work flow.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="subWorkFlowId">The sub work flow identifier.</param>
        /// <param name="newStatus">The new status.</param>
        /// <param name="requestModel">The request model.</param>
        /// <returns></returns>
        [HttpPut("{entitytype}/{entityid}/{statusWorkFlowId}/{subWorkFlowId}/{newStatus}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> MoveSubWorkFlow(string entityType, string entityId, string statusWorkFlowId, string subWorkFlowId, string newStatus, [FromBody]RequestModel requestModel)
        {
            try
            {
                return await ExecuteAsync(async () => { await EntityStatusService.MoveSubWorkFlow(entityType, entityId, statusWorkFlowId, subWorkFlowId, newStatus, requestModel); return NoContentResult; });
            }
            catch (IncompleteChecklistException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (TransitionNotAllowedException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Gets the checklist.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{statusWorkFlowId}/{status?}/checklist")]
        [ProducesResponseType(typeof(IEnumerable<IChecklist>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetChecklist(string entityType, string entityId, string statusWorkFlowId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetChecklist(entityType, entityId, statusWorkFlowId, status)));
        }

        /// <summary>
        /// Gets the checklist.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{status?}/checklist")]
        [ProducesResponseType(typeof(IChecklist[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetChecklist(string entityType, string entityId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetChecklist(entityType, entityId, status)));
        }

        /// <summary>
        /// Gets the transitions.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{statusWorkFlowId}/{status}/transitions")]
        [ProducesResponseType(typeof(IEnumerable<ITransition>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetTransitions(string entityType, string statusWorkFlowId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetTransitions(entityType, statusWorkFlowId, status)));
        }

        /// <summary>
        /// Gets the transitions.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/transitions")]
        [ProducesResponseType(typeof(ITransition[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetTransitions(string entityType, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetTransitions(entityType, status)));
        }

        /// <summary>
        /// Gets the reasons.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{statusWorkFlowId}/{status}/reasons")]
        [ProducesResponseType(typeof(IEnumerable<IReason>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetReasons(string entityType, string statusWorkFlowId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetReasons(entityType, statusWorkFlowId, status)));
        }

        /// <summary>
        /// Gets the reasons.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/reasons")]
        [ProducesResponseType(typeof(IReason[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetReasons(string entityType, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetReasons(entityType, status)));
        }

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{statusWorkFlowId}/{status}/activities")]
        [ProducesResponseType(typeof(IEnumerable<IActivity>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActivities(string entityType, string statusWorkFlowId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetActivities(entityType, statusWorkFlowId, status)));
        }

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/activities")]
        [ProducesResponseType(typeof(IActivity[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActivities(string entityType, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetActivities(entityType, status)));
        }

        /// <summary>
        /// Gets the status by status code.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{statusWorkFlowId}/{status}/detail")]
        [ProducesResponseType(typeof(IEnumerable<Status>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetStatusByStatusCode(string entityType, string statusWorkFlowId, string status)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetStatusByStatusCode(entityType, statusWorkFlowId, status)));
        }

        /// <summary>
        /// Gets the sub status details.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <param name="parentStatus">The parent status.</param>
        /// <param name="subWorkFlowId">The sub work flow identifier.</param>
        /// <param name="subStatus">The sub status.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{statusWorkFlowId}/{parentStatus}/{subWorkFlowId}/{subStatus}/detail")]
        [ProducesResponseType(typeof(ISubWorkFlow), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetSubStatusDetails(string entityType, string statusWorkFlowId, string parentStatus, string subWorkFlowId, string subStatus)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetSubStatusDetails(entityType, statusWorkFlowId, parentStatus, subWorkFlowId, subStatus)));
        }

        /// <summary>
        /// Gets the active status work flow.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/statusworkflow")]
        [ProducesResponseType(typeof(IStatusResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActiveStatusWorkFlow(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetActiveStatusWorkFlow(entityType, entityId)));
        }

        /// <summary>
        /// Gets the entity status history.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/history/{productId?}")] // Modify route as per review comment
        [ProducesResponseType(typeof(List<IEntityStatus>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetEntityStatusHistory(string entityType, string entityId, string productId)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetStatusTransitionHistory(entityType, entityId, productId)));
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="statusWorkFlowId">The status work flow identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{statusWorkFlowId}")]
        [ProducesResponseType(typeof(IStatusResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetStatus(string entityType, string entityId, string statusWorkFlowId)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetStatusByEntity(entityType, entityId, statusWorkFlowId)));
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(IStatusResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetStatus(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetStatusByEntity(entityType, entityId)));
        }

        /// <summary>
        /// Gets the work flow names.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        [HttpGet("{entitytype}/statusworkflows")]
        [ProducesResponseType(typeof(IEnumerable<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetWorkFlowNames(string entityType)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetWorkFlowNames(entityType)));
        }

        /// <summary>
        /// Gets the status by product.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{productId}/statusbyproduct")]
        [ProducesResponseType(typeof(IEnumerable<ITransition>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetStatusByProduct(string entityType, string entityId, string productId)
        {
            return await ExecuteAsync(async () => Ok(await EntityStatusService.GetStatusByProduct(entityType, entityId, productId)));
        }

        private static List<string> SplitReasons(string tags)
        {
            return string.IsNullOrWhiteSpace(tags)
                ? new List<string>()
                : tags.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}