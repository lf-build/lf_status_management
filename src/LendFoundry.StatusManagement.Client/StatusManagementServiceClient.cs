﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement.Client
{
    public class StatusManagementServiceClient : IEntityStatusService
    {
        public StatusManagementServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<StatusResponse>(request);
        }

        public async Task<IStatusResponse> GetStatusByEntity(string entityType, string entityId, string statusWorkFlowId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{statusWorkFlowId}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            return await Client.ExecuteAsync<StatusResponse>(request);
        }

        public async Task<Status> GetStatusByStatusCode(string entityType, string statusWorkFlowId, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/{productId}/detail", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<Status>(request);
        }

        public async Task ChangeStatus(string entityType, string entityId, string newStatus, IRequestModel requestModel)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{newStatus}", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("newStatus", newStatus);
            if (requestModel != null)
                request.AddJsonBody(requestModel);
            else
                request.AddJsonBody(new RequestModel());

            await Client.ExecuteAsync(request);
        }

        public async Task ChangeStatus(string entityType, string entityId, string statusWorkFlowId, string newStatus, IRequestModel requestModel)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{statusWorkFlowId}/{newStatus}", Method.PUT);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("newStatus", newStatus);
            if (requestModel != null)
                request.AddJsonBody(requestModel);
            else
                request.AddJsonBody(new RequestModel());

            await Client.ExecuteAsync(request);
        }

        public async Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string status)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{status?}/checklist", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            if (!string.IsNullOrWhiteSpace(status))
                request.AddUrlSegment("status", status);

            return await Client.ExecuteAsync<List<Checklist>>(request);
        }

        public async Task<IEnumerable<IChecklist>> GetChecklist(string entityType, string entityId, string statusWorkFlowId, string status)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{statusWorkFlowId}/{status?}/checklist", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            if (!string.IsNullOrWhiteSpace(status))
                request.AddUrlSegment("status", status);

            return await Client.ExecuteAsync<List<Checklist>>(request);
        }

        public async Task<IEnumerable<ITransition>> GetTransitions(string entityType, string status)
        {
            var request = new RestRequest("/{entityType}/{status}/transitions", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Transition>>(request);
        }

        public async Task<IEnumerable<ITransition>> GetTransitions(string entityType, string statusWorkFlowId, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/transitions", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Transition>>(request);
        }

        public async Task<IEnumerable<IReason>> GetReasons(string entityType, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/reasons", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Reason>>(request);
        }

        public async Task<IEnumerable<IReason>> GetReasons(string entityType, string statusWorkFlowId, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/reasons", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Reason>>(request);
        }

        public async Task<IEnumerable<IActivity>> GetActivities(string entityType, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/activities", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Activity>>(request);
        }

        public async Task<IEnumerable<IActivity>> GetActivities(string entityType, string statusWorkFlowId, string status)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{status}/activities", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<List<Activity>>(request);
        }


        public async Task<List<IEntityStatus>> GetStatusTransitionHistory(string entityType, string entityId, string productId = null)
        {
            string uri = string.Empty;
            if (productId != null)
                uri = "{entityType}/{entityId}/history/{productId}";
            else
                uri = "{entityType}/{entityId}/history";
            var request = new RestRequest(uri, Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            if (productId != null) request.AddUrlSegment(nameof(productId), productId);
            var result = await Client.ExecuteAsync<List<EntityStatus>>(request);
            return new List<IEntityStatus>(result);
        }

        public async Task InitiateSubWorkFlow(string entityType, string entityId, string subWorkFlowId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/subworkflow/{subWorkFlowId}", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("subWorkFlowId", subWorkFlowId);
            await Client.ExecuteAsync(request);
        }

        public async Task ProcessStatusWorkFlow(string entityType, string entityId, string productId, string statusWorkFlowId, WorkFlowStatus status)
        {
            var request = new RestRequest("{entitytype}/{entityid}/{productId}/workflow/{statusWorkFlowId}/{status}", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("status", status.ToString());
            await Client.ExecuteAsync(request);
        }

        public async Task MoveSubWorkFlow(string entityType, string entityId, string statusWorkFlowId, string subWorkFlowId, string newStatus, IRequestModel requestModel)
        {
            var request = new RestRequest("{entitytype}/{entityid}/{statusWorkFlowId}/{subWorkFlowId}/{newStatus}", Method.PUT);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("subWorkFlowId", subWorkFlowId);
            request.AddUrlSegment("newStatus", newStatus);
            if (requestModel != null)
                request.AddJsonBody(requestModel);
            else
                request.AddJsonBody(new RequestModel());

            await Client.ExecuteAsync(request);
        }


        public async Task<IEnumerable<string>> GetWorkFlowNames(string entityType)
        {
            var request = new RestRequest("{entitytype}/statusworkflows", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            return await Client.ExecuteAsync<IEnumerable<string>>(request);
        }

        public async Task<IStatusResponse> GetActiveStatusWorkFlow(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/statusworkflow", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<StatusResponse>(request);
        }

        public async Task<ISubWorkFlow> GetSubStatusDetails(string entityType, string statusWorkFlowId, string parentStatus, string subWorkFlowId, string subStatus)
        {
            var request = new RestRequest("/{entityType}/{statusWorkFlowId}/{parentStatus}/{subWorkFlowId}/{subStatus}/detail", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("statusWorkFlowId", statusWorkFlowId);
            request.AddUrlSegment("parentStatus", parentStatus);
            request.AddUrlSegment("subWorkFlowId", subWorkFlowId);
            request.AddUrlSegment("subStatus", subStatus);
            return await Client.ExecuteAsync<SubWorkFlow>(request);
        }

        public async Task<List<IEntityStatus>> GetStatusByProduct(string entityType, string entityId, string productId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{productId}/statusbyproduct", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<EntityStatus>>(request);
            return new List<IEntityStatus>(result);
        }
    }
}
