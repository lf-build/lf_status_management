using LendFoundry.Security.Tokens;

namespace LendFoundry.StatusManagement.Client
{
    public interface IStatusManagementServiceFactory
    {
        IEntityStatusService Create(ITokenReader reader);
    }
}