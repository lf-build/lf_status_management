using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.StatusManagement.Client
{
    public static class StatusManagementServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddStatusManagementService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IStatusManagementServiceFactory>(p => new StatusManagementServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IStatusManagementServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddStatusManagementService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IStatusManagementServiceFactory>(p => new StatusManagementServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IStatusManagementServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddStatusManagementService(this IServiceCollection services)
        {
            services.AddTransient<IStatusManagementServiceFactory>(p => new StatusManagementServiceFactory(p));
            services.AddTransient(p => p.GetService<IStatusManagementServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}