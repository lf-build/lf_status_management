﻿using System.Collections.Generic;
using Xunit;

namespace LendFoundry.StatusManagement.Tests
{
    public class StatusManagementClientTest
    {
        [Fact]
        public void GetStatusByApplicationNumberWhenExecutionOk()
        {
	        var serviceClient = new Fakes.FakeServiceClient<StatusResponse> {InMemoryData = new StatusResponse()};
	        var client = new Client.StatusManagementServiceClient(serviceClient);

            var result = client.GetStatusByEntity("loan","app001");
            Assert.NotNull(result);
            Assert.IsType(typeof(StatusResponse), result);
        }

        [Fact]
        public void GetChecklistWhenExecutionOk()
        {
	        var serviceClient = new Fakes.FakeServiceClient<List<IChecklist>>
	        {
		        InMemoryData = new List<Checklist>
		        {
			        new Checklist(),
			        new Checklist()
		        }
	        };
	        var client = new Client.StatusManagementServiceClient(serviceClient);

            var result = client.GetChecklist("loan", "app001", "100.03");
            Assert.NotNull(result);
        }

        [Fact]
        public void GetTransitionsWhenExecutionOk()
        {
	        var serviceClient = new Fakes.FakeServiceClient<List<Transition>>
	        {
		        InMemoryData = new List<Transition>
		        {
			        new Transition(),
			        new Transition()
		        }
	        };
	        var client = new Client.StatusManagementServiceClient(serviceClient);

            var result = client.GetTransitions("loan", "100.03");
            Assert.NotNull(result);
        }

        [Fact]
        public void GetReasonsWhenExecutionOk()
        {
	        var serviceClient = new Fakes.FakeServiceClient<List<Reason>>
	        {
		        InMemoryData = new List<Reason>
		        {
			        new Reason(),
			        new Reason()
		        }
	        };
	        var client = new Client.StatusManagementServiceClient(serviceClient);

            var result = client.GetReasons("loan", "100.03");
            Assert.NotNull(result);
        }

        [Fact]
        public void GetActivitiesWhenExecutionOk()
        {
	        var serviceClient = new Fakes.FakeServiceClient<List<Activity>>
	        {
		        InMemoryData = new List<Activity>
		        {
			        new Activity(),
			        new Activity()
		        }
	        };
	        var client = new Client.StatusManagementServiceClient(serviceClient);

            var result = client.GetActivities("loan", "100.03");
            Assert.NotNull(result);
        }
    }
}
