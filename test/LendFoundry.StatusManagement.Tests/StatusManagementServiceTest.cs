﻿using LendFoundry.StatusManagement.Configuration;
using LendFoundry.StatusManagement.Tests.Fakes;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Clients.DecisionEngine;
using Xunit;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;


namespace LendFoundry.StatusManagement.Tests
{
    public class StatusManagementServiceTest
    {
        [Fact]
        public void InitServiceWhenNoDecisionEngineInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
            new StatusManagementService
            (
                null,
                Mock.Of<IStatusManagementRepository>(),
                Mock.Of<IConfiguration>(),
                Mock.Of<IEventHubClient>(),
                Mock.Of<ILookupService>(),
                Mock.Of<ITenantTime>(),
                Mock.Of<ILogger>(),
                Mock.Of<IStatusDataRepository>(),
                Mock.Of<ITokenReader>(),
                Mock.Of<ITokenHandler>(),
                Mock.Of<IStatusManagementHistoryRepository>()
            
                );
            });
        }

        [Fact]
        public void InitServiceWhenNoRepositoryInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new StatusManagementService
                (
                    Mock.Of<IDecisionEngineService>(),
                    null,
                    Mock.Of<IConfiguration>(),
                    Mock.Of<IEventHubClient>(),
                    Mock.Of<ILookupService>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<IStatusDataRepository>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IStatusManagementHistoryRepository>()
                );
            });
        }

        [Fact]
        public void InitServiceWhenNoConfigurationInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new StatusManagementService
                (
                    Mock.Of<IDecisionEngineService>(),
                    Mock.Of<IStatusManagementRepository>(),
                    null,
                    Mock.Of<IEventHubClient>(),
                    Mock.Of<ILookupService>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<IStatusDataRepository>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IStatusManagementHistoryRepository>()
                );
            });
        }

        [Fact]
        public void InitServiceWhenNoEventhubInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
            new StatusManagementService
            (
                Mock.Of<IDecisionEngineService>(),
                Mock.Of<IStatusManagementRepository>(),
                Mock.Of<IConfiguration>(),
                null,
                Mock.Of<ILookupService>(),
                Mock.Of<ITenantTime>(),
                Mock.Of<ILogger>(),
                Mock.Of<IStatusDataRepository>(),
                Mock.Of<ITokenReader>(),
                Mock.Of<ITokenHandler>(),
                Mock.Of<IStatusManagementHistoryRepository>()
                );
            });
        }

        [Fact]
        public void InitServiceWhenNoLookupInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new StatusManagementService
                (
                    Mock.Of<IDecisionEngineService>(),
                    Mock.Of<IStatusManagementRepository>(),
                    Mock.Of<IConfiguration>(),
                    Mock.Of<IEventHubClient>(),
                    null,
                    Mock.Of<ITenantTime>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<IStatusDataRepository>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IStatusManagementHistoryRepository>()
                );
            });
        }
        [Fact]
        public void InitServiceWhenNoTenantTimeInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new StatusManagementService
                (
                    Mock.Of<IDecisionEngineService>(),
                    Mock.Of<IStatusManagementRepository>(),
                    Mock.Of<IConfiguration>(),
                    Mock.Of<IEventHubClient>(),
                    Mock.Of<ILookupService>(),
                    null,
                    Mock.Of<ILogger>(),
                    Mock.Of<IStatusDataRepository>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IStatusManagementHistoryRepository>()
                );
            });
        }
        [Fact]
        public void GetStatusByEntityWhenValuesFound()
        {
            var repository = new FakeRepository();
            var data = Mock.Of<IEntityStatus>();
            data.EntityType = "loan";
            data.EntityId = "app001";
            data.Status = "200.02";
            repository.InMemoryData = new[] { data }.ToList();

            var service = GetService(repository);
            var result = service.GetStatusByEntity(data.EntityType, data.EntityId);

            Assert.NotNull(result);
            Assert.True(result.Code == data.Status);
        }

        [Fact]
        public void GetStatusByEntityWhenNoValuesFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityType = "loan";
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var service = GetService(repository);
                var result = service.GetStatusByEntity("loan", "app002");
            });
        }

        [Fact]
        public void GetStatusByEntityWhenNoEntityTypeInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetStatusByEntity(string.Empty, "123");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetStatusByEntity("", "123");
            });
        }

        [Fact]
        public void GetStatusByEntityWhenNoEntityIdInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetStatusByEntity("loan", string.Empty);
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetStatusByEntity("loan", "");
            });
        }

        [Fact]
        public void GetStatusByEntityWhenHasInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var config = new Configuration.Configuration
                {
                    EntityTypes = new Dictionary<string, Entity>()
                };
                config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
                {
                    Statuses = new List<Status>
                    {
                        new Status{ Code = "200.01" }
                    }
                }));

                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityType = "loan";
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var service = GetService(repository, null, config);
                var result = service.GetStatusByEntity(data.EntityType, data.EntityId);
            });
        }

        [Fact]
        public void GetStatusByEntityWhenEntityDoesNotExists()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.GetStatusByEntity("xxxxx", "123456");
            });
        }

        [Fact]
        public void ChangeStatusWhenHasInvalidNewStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.ChangeStatus("loan", "app001", string.Empty, new List<string> { "some-reason" });
            });
        }

        [Fact]
        public void ChangeStatusWhenHasInvalidApplicationNumber()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.ChangeStatus(string.Empty, string.Empty, "100.05", new List<string> { "some-reason" });
            });
        }

        [Fact]
        public void ChangeStatusWhenHasInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var config = new Configuration.Configuration
                {
                    EntityTypes = new Dictionary<string, Entity>()
                };
                config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
                {
                    Statuses = new List<Status>
                    {
                        new Status{ Code = "200.01" }
                    }
                }));

                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityType = "loan";
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var service = GetService(repository, null, config);
                service.ChangeStatus(data.EntityType, data.EntityId, data.Status, new List<string>());
            });
        }

        //[Fact]
        //public void ChangeStatusWhenValuesFound()
        //{
        //    var decisionEngine = new Mock<IDecisionEngineService>();
        //    var result = new DecisionEngineResult<bool>();
        //    result.Output = true;
        //    decisionEngine.Setup(c =>
        //    c.Execute<dynamic, bool>(
        //            It.IsAny<string>(),
        //            It.IsAny<string>(),
        //            It.IsAny<object>()))
        //            .Returns(result);

        //    var eventHub = new Mock<IEventHubClient>();
        //    eventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<StatusChanged>()));

        //    var config = new Configuration.Configuration
        //    {
        //        EntityTypes = new Dictionary<string, Entity>()
        //    };
        //    config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
        //    {
        //        Statuses = new List<Status>
        //            {
        //                new Status
        //                {
        //                    Code = "200.01",
        //                    Transitions = new[] { "200.02", "200.03" },
        //                    Reasons = new Dictionary<string, string>
        //                    {
        //                        {"key-a", "reason-a"}
        //                    },
        //                    Checklist = new [] { "check-item-a" }
        //                },
        //                new Status
        //                {
        //                    Code = "200.02",
        //                    Transitions = new[] { "200.03", "200.04" },
        //                    Reasons = new Dictionary<string, string>
        //                    {
        //                        {"key-a", "reason-a"}
        //                    },
        //                    Checklist = new [] { "check-item-a" }
        //                }
        //            },
        //        Checklist = new List<Configuration.Checklist>
        //            {
        //                new Configuration.Checklist { Code = "check-item-a", RuleName = "checkItem", RuleVersion = "1.0" },
        //                new Configuration.Checklist { Code = "check-item-b", RuleName = "checkItem", RuleVersion = "1.0" }
        //            }
        //    }));

        //    var repository = new FakeRepository();
        //    var statusA = Mock.Of<IEntityStatus>();
        //    statusA.EntityType = "loan";
        //    statusA.EntityId = "app001";
        //    statusA.Status = "200.01";
        //    var statusB = Mock.Of<IEntityStatus>();
        //    statusB.EntityType = "marchant";
        //    statusB.EntityId = "app001";
        //    statusB.Status = "200.02";
        //    repository.InMemoryData = new[] { statusA, statusB }.ToList();

        //    var service = GetService(repository, eventHub.Object, config, decisionEngine.Object);
        //    service.ChangeStatus(statusA.EntityType, statusA.EntityId, statusB.Status, new List<string> { "key-a" });

        //    eventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<StatusChanged>()));
        //}

        [Fact]
        public void GetChecklistWhenHasInvalidApplicationNumber()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                var result = service.GetChecklist(string.Empty, string.Empty, "200.01");
                Assert.Null(result);
            });
        }

        [Fact]
        public void GetChecklistWhenHasUnmatchedApplicationNumber()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityType = "loan";
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var service = GetService(repository);
                var result = service.GetChecklist("loan", "app002", string.Empty);
                Assert.Null(result);
            });
        }

        [Fact]
        public void GetChecklistWhenHasNoChecklistInStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityType = "loan";
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var config = new Configuration.Configuration
                {
                    EntityTypes = new Dictionary<string, Entity>()
                };
                config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
                {
                    Statuses = new List<Status>
                    {
                        new Status{ Code = "200.01", Checklist = null}
                    },
                    Checklist = new List<Configuration.Checklist>
                    {
                        new Configuration.Checklist { Code = "check-item-a", RuleName = "checkItem", RuleVersion = "1.0" },
                        new Configuration.Checklist { Code = "check-item-b", RuleName = "checkItem", RuleVersion = "1.0" }
                    }
                }));

                var service = GetService(repository, null, config);
                var result = service.GetChecklist(data.EntityType, data.EntityId, string.Empty);
                Assert.Empty(result);
            });
        }

        //[Fact]
        //public void GetChecklistWhenInvalidStatusButValidApplicationNumber()
        //{
        //    var decisionEngine = new Mock<IDecisionEngineService>();
        //    var result = new DecisionEngineResult<bool>();
        //    result.Output = false;
        //    decisionEngine.Setup(c =>
        //    c.Execute<dynamic, bool>(
        //            It.IsAny<string>(),
        //            It.IsAny<string>(),
        //            It.IsAny<object>()))
        //            .Returns(result);

        //    var repository = new FakeRepository();
        //    var data = Mock.Of<IEntityStatus>();
        //    data.EntityType = "loan";
        //    data.EntityId = "app001";
        //    data.Status = "200.02";
        //    repository.InMemoryData = new[] { data }.ToList();

        //    var config = new Configuration.Configuration
        //    {
        //        EntityTypes = new Dictionary<string, Entity>()
        //    };
        //    config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
        //    {
        //        Statuses = new List<Status>
        //        {
        //            new Status{ Code = "200.02", Checklist = new[] { "check-item-a" }}
        //        },
        //        Checklist = new List<Configuration.Checklist>
        //        {
        //            new Configuration.Checklist { Code = "check-item-a", RuleName = "checkItem", RuleVersion = "1.0" },
        //            new Configuration.Checklist { Code = "check-item-b", RuleName = "checkItem", RuleVersion = "1.0" }
        //        }
        //    }));

        //    var service = GetService(repository, null, config, decisionEngine.Object);
        //    var checklist = service.GetChecklist(data.EntityType, data.EntityId, string.Empty);
        //    Assert.NotEmpty(checklist);
        //    Assert.True(checklist.Any(r => r.Code == "check-item-a"));
        //}

        [Fact]
        public void GetChecklistWhenInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                var result = service.GetChecklist("loan", "app001", "@#$%¨&*");
                Assert.Empty(result);
            });
        }

        [Fact]
        public void GetTransitionsWhenHasInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                var result = service.GetTransitions("loan", "@#$%¨&*");
                Assert.Empty(result);
            });
        }

        [Fact]
        public void GetTransitionsWhenValuesFound()
        {
            var repository = new FakeRepository();
            var data = Mock.Of<IEntityStatus>();
            data.EntityId = "app001";
            data.Status = "200.02";
            repository.InMemoryData = new[] { data }.ToList();

            var config = new Configuration.Configuration
            {
                EntityTypes = new Dictionary<string, Entity>()
            };
            config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
            {
                Statuses = new List<Status>
                    {
                         new Status
                        {
                            Code = "200.02",
                            Checklist = new[] { "check-item-a" },
                            Transitions = new[] { "200.03", "200.04" }
                        },
                        new Status
                        {
                            Code = "200.03",
                            Checklist = new[] { "check-item-a" },
                            //Transitions = new[] { "200.03", "200.04" }
                        },
                        new Status
                        {
                            Code = "200.04",
                            Checklist = new[] { "check-item-a" },
                            //Transitions = new[] { "200.03", "200.04" }
                        }
                    },
                Checklist = new List<Configuration.Checklist>
                    {
                        new Configuration.Checklist() { Code = "check-item-a" },
                        new Configuration.Checklist() { Code = "check-item-b" }
                    }
            }));

            var service = GetService(null, null, config);
            var result = service.GetTransitions("loan", "200.02");
            Assert.NotEmpty(result);
            Assert.True(result.Any(r => r.Code == "200.03"));
            Assert.True(result.Any(r => r.Code == "200.04"));
            Assert.True(!result.Any(r => r.Code == "200.05"));
        }

        [Fact]
        public void GetReasonsWhenHasInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                var result = service.GetReasons("loan", "@#$%¨&*");
                Assert.Empty(result);
            });
        }

        [Fact]
        public void GetReasonsWhenValuesFound()
        {
            var repository = new FakeRepository();
            var data = Mock.Of<IEntityStatus>();
            data.EntityId = "app001";
            data.Status = "200.02";
            repository.InMemoryData = new[] { data }.ToList();

            var config = new Configuration.Configuration
            {
                EntityTypes = new Dictionary<string, Entity>()
            };
            config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
            {
                Statuses = new List<Status>
                    {
                         new Status
                        {
                            Code = "200.02",
                            Checklist = new[] { "check-item-a" },
                            Reasons = new Dictionary<string, string>
                            {
                                { "key", "value"}
                            }
                        }
                    },
                Checklist = new List<Configuration.Checklist>
                    {
                        new Configuration.Checklist() { Code = "check-item-a" },
                        new Configuration.Checklist() { Code = "check-item-b" }
                    }
            }));

            var service = GetService(repository, null, config);
            var result = service.GetReasons("loan", "200.02");
            Assert.NotEmpty(result);
            Assert.True(result.Any(r => r.Code == "key"));
        }

        [Fact]
        public void GetActivitiesWhenHasInvalidStatus()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                var result = service.GetActivities("loan", "@#$%¨&*");
                Assert.Empty(result);
            });
        }

        [Fact]
        public void GetActivitiesWhenHasValuesFound()
        {
            var repository = new FakeRepository();
            var data = Mock.Of<IEntityStatus>();
            data.EntityType = "loan";
            data.EntityId = "app001";
            data.Status = "200.02";
            repository.InMemoryData = new[] { data }.ToList();

            var config = new Configuration.Configuration
            {
                EntityTypes = new Dictionary<string, Entity>()
            };
            config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
            {
                Statuses = new List<Status>
                    {
                         new Status
                        {
                            Code = "200.02",
                            Checklist = new[] { "check-item-a" },
                            Activities = new Dictionary<string, string>
                            {
                                { "key-a", "value" },
                                { "key-b", "value" }
                            },
                        }
                    }
            }));

            var service = GetService(repository, null, config);
            var result = service.GetActivities("loan", "200.02");
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void GetActivitiesWhenNoValuesFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var repository = new FakeRepository();
                var data = Mock.Of<IEntityStatus>();
                data.EntityId = "app001";
                data.Status = "200.02";
                repository.InMemoryData = new[] { data }.ToList();

                var config = new Configuration.Configuration
                {
                    EntityTypes = new Dictionary<string, Entity>()
                };
                config.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity()
                {
                    Statuses = new List<Status>
                    {
                         new Status
                        {
                            Code = "200.02",
                            Checklist = new[] { "check-item-a" },
                            Reasons = new Dictionary<string, string> {}
                        }
                    }
                }));

                var service = GetService(repository, null, config);
                var result = service.GetActivities("loan", "200.02");
                Assert.Empty(result);
            });
        }

        private IEntityStatusService GetService
        (
            IStatusManagementRepository repository = null,
            IEventHubClient eventhub = null,
            IConfiguration configuration = null,
            IDecisionEngineService decisionEngine = null,
            ILookupService lookup = null,
            ITenantTime tenantTime = null
        )
        {
            var inTimeRepository = repository ?? new FakeRepository();

            var inTimeConfiguration = configuration;

            if (inTimeConfiguration == null)
            {
                inTimeConfiguration = new Configuration.Configuration
                {
                    EntityTypes = new Dictionary<string, Entity>()
                };

                inTimeConfiguration.EntityTypes.Add(new KeyValuePair<string, Entity>("loan", new Entity
                {
                    Checklist = new List<Configuration.Checklist>(),
                    Statuses = new List<Status>
                {
                    new Status
                    {
                        Code = "200.01",
                        Name = "Processing",
                        Style = "Green",
                        Label = "Processing",
                        Transitions = new [] {"200.02", "200.03", "200.04", "200.05", "200.06", "300.02", "300.05" },
                        Activities = null,
                        Checklist = new[] { "check-item-a", "check-item-b", "check-item-c" }
                    },
                    new Status
                    {
                        Code = "200.02",
                        Name = "Processing - OfferGenerated",
                        Style = "Green",
                        Label = "Processing - OfferGenerated",
                        Transitions = new [] { "100.04", "100.07", "100.08", "100.09", "200.01", "200.02", "200.03", "200.04", "200.05", "200.06", "300.02", "300.05" },
                        Activities = null,
                        Checklist = new[] { "check-item-d", "check-item-e", "check-item-f" }
                    }
                }
                }));
            }

            IEventHubClient inTimeEventhub = eventhub;
            if (inTimeEventhub == null)
                inTimeEventhub = new FakeEventHub();

            IDecisionEngineService inTimeDecisionEngine = decisionEngine;
            if (inTimeDecisionEngine == null)
                inTimeDecisionEngine = Mock.Of<IDecisionEngineService>();

            ILookupService inTimeLookup = lookup;
            if (inTimeLookup == null)
            {
                var mockLookup = new Mock<ILookupService>();
                mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>
                {
                    { "loan","loan" },
                    { "merchant","merchant" },
                    { "application","application" },
                    { "borrower","borrower" },
                });
                inTimeLookup = mockLookup.Object;
            }
            ITenantTime inTenantTime = tenantTime;
            if (inTenantTime == null)
                inTenantTime = Mock.Of<ITenantTime>();
            return new StatusManagementService(inTimeDecisionEngine, inTimeRepository, inTimeConfiguration, inTimeEventhub, inTimeLookup, inTenantTime, Mock.Of<ILogger>(), Mock.Of<IStatusDataRepository>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IStatusManagementHistoryRepository>());
        }
    }
}
